const express = require("express");
const { addFriend, getFriends } = require("../controllers/friends.js");

const router = express.Router();
const { protectRoute } = require("../middleware/auth.js");

router.route("/add-friend").post(protectRoute, addFriend);
router.route("/get-friends").get(protectRoute, getFriends)

module.exports = router;