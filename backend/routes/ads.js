const express = require("express");
const { addAd, getAds, getAd, getUsersAds, deleteAd } = require("../controllers/ads.js");

const router = express.Router();
const { protectRoute } = require("../middleware/auth.js");

router.route("/add-ad").post(protectRoute, addAd);
router.route("/get-ads").get(protectRoute, getAds)
router.route("/get-ad").post(protectRoute, getAd)
router.route("/delete-ad").delete(protectRoute, deleteAd)
router.route("/get-users-ads").post(protectRoute, getUsersAds)

module.exports = router;