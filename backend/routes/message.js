const express = require("express");
const { sendMessage, getMessages, getConversations } = require("../controllers/message.js");

const router = express.Router();
const { protectRoute } = require("../middleware/auth.js");

router.route("/send-message").post(protectRoute, sendMessage);
router.route("/get-messages").post(protectRoute, getMessages)
router.route("/get-conversations").get(protectRoute, getConversations)

module.exports = router;