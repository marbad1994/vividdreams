const express = require("express");
const { registerUser, loginUser, getMe, logout, refreshToken } = require("../controllers/auth.js");
const { getOnlineUsers, getOnlineUser, getStats } = require("../controllers/notice.js");

const router = express.Router();
const { protectRoute } = require("../middleware/auth.js");

router.route("/register").post(registerUser);

router.route("/login").post(loginUser);
router.route("/logout").get(logout);
router.route("/me").get(protectRoute, getMe);
router.route("/refresh-token").get(protectRoute, refreshToken)

router.route("/get-online-users").get(protectRoute, getOnlineUsers)
router.route("/get-online-user").post(protectRoute, getOnlineUser)
router.route("/get-stats").get(protectRoute, getStats)

module.exports = router;
