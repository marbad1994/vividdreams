const express = require("express");
const { getProfile, editProfile } = require("../controllers/profile.js");
const { uploadProfilePicture, uploadImageToGallery, getProfilePicture, getImages, deleteImageInGallery, editImageInGallery, likeImage} = require("../controllers/profilePicture.js")

const router = express.Router();
const { protectRoute } = require("../middleware/auth.js");

router.route("/get-profile").post(protectRoute, getProfile);

router.route("/edit-profile").post(protectRoute, editProfile)

router.route("/upload-image").post(protectRoute, uploadProfilePicture)

router.route("/profile-picture/:name").get(protectRoute, getProfilePicture)

router.route("/upload-image-to-gallery").post(protectRoute, uploadImageToGallery)

router.route("/image-gallery/:name").get(protectRoute, getImages)

router.route("/delete-image-in-gallery").post(protectRoute, deleteImageInGallery)

router.route("/edit-image-in-gallery").post(protectRoute, editImageInGallery)

router.route("/like-image").post(protectRoute, likeImage)


module.exports = router;