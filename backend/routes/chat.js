const express = require("express");
const { getChat, saveChat } = require("../controllers/chat.js");

const router = express.Router();
const { protectRoute } = require("../middleware/auth.js");

router.route("/get-chat").post(protectRoute, getChat);

router.route("/save-chat").post(protectRoute, saveChat)

module.exports = router;