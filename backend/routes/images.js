const express = require("express");
const { uploadProfilePicture, getProfilePicture } = require("../controllers/profilePicture.js");

const router = express.Router();
const { protectRoute } = require("../middleware/auth.js");

router.route("/upload-profile-picture").post(protectRoute, uploadProfilePicture);
router.route("/get-profile-picture").get(protectRoute, getProfilePicture)

module.exports = router;