const express = require("express");
const { counties, cities } = require("../controllers/geo.js");

const router = express.Router();

router.route("/get-counties").get(counties);

router.route("/get-cities").post(cities)

module.exports = router;