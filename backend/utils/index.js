const jwt = require("jsonwebtoken");
const userModel = require("../models/auth.js");

exports.getUser = async(req) => {
    let token;
    if (
        req.headers.authorization &&
        req.headers.authorization.startsWith("Bearer")
    ) {
        token = req.headers.authorization.split(" ")[1];
    }
    //make sure token exits

    if (!token) {
        return false
    }
    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await userModel.findById(decoded.id);
        return user
    } catch (error) {
        return false
    }
}