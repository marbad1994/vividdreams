const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");



const Message = new mongoose.Schema({
    username: {
        type: String
    },
    text: {
        type: String
    },
    timestamp: {
        type: Date
    }
})

const ChatSchema = new mongoose.Schema({
    roomname: {
        type: mongoose.Schema.ObjectId,
        unique: true,
        required: true
    },
    chat: {
        type: [Message]
    }
})

// RoomSchema.index({ memberOne: 1, memberTwo: 1 }, { unique: true })
module.exports = mongoose.model("Chat", ChatSchema);