const mongoose = require("mongoose");

const ImageSchema = new mongoose.Schema({
    title: {
        type: String
    },
    description: {
        type: String,
    },
    likes: {
        type: [String],
        required: true,
        default: [],
    },
    imgPath: {
        type: String,
        required: true,
    },
    availableFor: {
        type: String,
        required: true,
        default: 'all',
    }
})

const InfoSchema = new mongoose.Schema({
    lastLoggedIn: {
        type: Date
    },
    online: {
        type: Boolean,
    },
    gender: {
        type: String,
        required: true
    },
    sexuality: {
        type: String,
        required: true
    },
    etnicity: {
        type: String
    },
    height: {
        type: Number
    },
    weight: {
        type: Number
    },
    age: {
        type: Number,
        required: true
    },
    bodyType: {
        type: String
    }
})


const ProfileSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.ObjectId,
        unique: true,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    city: {
        type: String,
        required: true
    },
    county: {
        type: String,
        required: true
    },
    about: {
        type: String
    },
    lookingFor: {
        type: String 
    },
    info: {
        type: InfoSchema
    },
    images: {
        type: [ImageSchema]
    },
    interests: {
        type: [String]
    },
    profilePicture: {
        type: String
    }
})

module.exports = mongoose.model("Profile", ProfileSchema);