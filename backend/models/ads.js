const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const AdSchema = new mongoose.Schema({
    user: {
        type: String,
        required: [true, "Please add your name"],
    },
    county: {
        type: String,
        required: [true, "Please add county"],
    },
    city: {
        type: String,
        required: [true, "Please add city"],
    },
    adType: {
        type: String,
        required: [true, "Please add adType"],
    },
    subject: {
        type: String,
        required: [true, "Please add subject"],
    },
    description: {
        type: String,
        required: [true, "Please add description"],
    },
    ageFrom: {
        type: Number,
    },
    ageTo: {
        type: Number,
    },
    visited: {
        type: Number,
    },
    responses: {
        type: Number,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },

});


module.exports = mongoose.model("Ads", AdSchema);