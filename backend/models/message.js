const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const MessageSchema = new mongoose.Schema({
    user: {
        type: String,
        required: [true, "Please add your name"],
    },
    read: {
        type: Boolean,
    },
    recipient: {
        type: String,
        required: [true, "Please add your recipient's name"],
    },
    message: {
        type: String,
        required: [true, "Please add your message"],
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },

});


module.exports = mongoose.model("Message", MessageSchema);