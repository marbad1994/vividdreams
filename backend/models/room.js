const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");

const member = new mongoose.Schema({
    name: {
        type: String
    },
    connected: {
        type: Boolean
    },
    connectionId: {
        type: String
    }
})


const RoomSchema = new mongoose.Schema({

    members: {
        type: [member],
        unique: true,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});




// RoomSchema.index({ memberOne: 1, memberTwo: 1 }, { unique: true })
module.exports = mongoose.model("Room", RoomSchema);
