const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const ProfilePictureSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "Please add your name"],
    },
    profilePicture: {
        type: Buffer,
        required: [true, "Please add image"],
    }

});

ProfilePictureSchema.index({ username: 1 }, { unique: true })

module.exports = mongoose.model("ProfilePicture", ProfilePictureSchema);