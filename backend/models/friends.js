const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const FriendSchema = new mongoose.Schema({
    user: {
        type: String,
        required: [true, "Please add your name"],
    },
    friend: {
        type: String,
        required: [true, "Please add friend"],
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },

});

FriendSchema.index({ friend: 1, user: 1 }, { unique: true })

module.exports = mongoose.model("Friends", FriendSchema);