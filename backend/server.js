const express = require("express");

const fileupload = require("express-fileupload");
const bodyParser = require('body-parser');

const dotenv = require("dotenv");
const app = express();
const socket = require("socket.io");
const morgan = require("morgan");
const color = require("colors");
const cors = require("cors");
const connectDB = require("./config/db.js");
const { getCurrentUser, userLeave, userJoin } = require("./dummyuser");
const { updateConnection, getConnectionId, getUserFromConnectionId, saveChat } = require("./controllers/chat.js")
const errorHandler = require("./middleware/errorhandler.js");
const { getUser } = require("./utils");

dotenv.config({ path: "../backend/config/config.env" });

const auth = require("./routes/auth.js");
const chatRoute = require("./routes/chat.js")
const friendsRoute = require("./routes/friends")
const profileRoute = require("./routes/profile")
const geoRoute = require("./routes/geo.js")
const imagesRoute = require("./routes/images.js")
const adsRoute = require("./routes/ads")
const messageRoute = require("./routes/message")
app.use(cors());
app.use(fileupload());
app.use(express.static("files"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.disable('x-powered-by');

const apiUrl = (endpoint) => {
    if (endpoint.slice(0, 1) != "/") {
        endpoint = `/${endpoint}`
    }
    return `/api/v1${endpoint}`
}

app.use(morgan("dev"));
app.use(express.json());
app.use(apiUrl("/auth"), auth);
app.use(apiUrl("/chat"), chatRoute)
app.use(apiUrl("/friends"), friendsRoute)
app.use(apiUrl("/profile"), profileRoute)
app.use(apiUrl("/geo"), geoRoute)
app.use(apiUrl("/images"), imagesRoute)
app.use(apiUrl("/ads"), adsRoute)
app.use(apiUrl("/message"), messageRoute)
app.use(errorHandler);
connectDB();

const port = process.env.PORT || 5000;

var server = app.listen(
    port,
    console.log(
        `Server is running in ${process.env.NODE_ENV} on port ${process.env.PORT} `
        .yellow.bold
    )
);
process.on("unhandledRejection", (err, promisee) => {
    console.log(`Error : ${err.message}`.red.bold);
    server.close(() => process.exit(1));
});

const io = socket(server);

io.on("connection", (socket) => {
    socket.on("joinRoom", async({ user, roomId }) => {
        await updateConnection(user.name, roomId, true, socket.id)

        socket.join(roomId);

    });

    socket.on("chat", async(info) => {
        const id = socket.id
        const data = await getUserFromConnectionId(info.roomname, info.username)
        let user = data.user
        let roomname = info.roomname
        let chat = info.chat
        if (user) {
            // console.log(await saveChat(roomname, chat))
            io.to(roomname).emit("message", {
                userId: user.connectionId,
                username: user.name,
                text: info.text,
                roomname: roomname

            });
        }

    });

    // Disconnect , when user leave room
    socket.on("disconnect", () => {
        // * delete user from users & emit that user has left the chat
        const user = userLeave(socket.id);

        if (user) {
            io.to(user.room).emit("message", {
                userId: user.connectionId,
                username: user.name,
                text: `${user.name} has left the chat`,
            });
        }
    });
});