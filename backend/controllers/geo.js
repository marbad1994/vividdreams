const request = require('request');

exports.counties = async(req, res, next) => {
    try {
        let data = {
            Blekinge: "A",
            Skåne: "B",
            Halland: "C"
        }
        res.status(200).json({
            success: true,
            data: data
        })
    } catch(error) {
        next(error)
    }
}

exports.cities = async(req, res, next) => {
    try {
        const countyCode = req.body.countyCode;
    let data = {
        A: ["Ronneby", "Karlskrona", "Karlshamn"],
        B: ["Malmö", "Helsingborg", "Ystad"],
        C: ["Laholm", "Halmstad", "Varberg"]
    }
    res.status(200).json({
        success: true,
        data: data[countyCode]
    })
} catch(error) {
    next(error)
}
}

exports.counties_old = async(req, res, next) => {
    try {
        const options = {
            method: 'GET',
            url: 'https://wft-geo-db.p.rapidapi.com/v1/geo/countries/SE/regions',
            headers: {
            'x-rapidapi-key': '6f882fb5b5mshb29ecdaca69481fp111c6ajsn4577c25cf140',
            'x-rapidapi-host': 'wft-geo-db.p.rapidapi.com',
            useQueryString: true
            }
        };
        let counties = {}
        request(options, function (error, response, body) {
            // if (error) throw new Error(error);
            if (error) {
                res.status(401).json({
                    success: false
                })
            }
            
            const { data } = JSON.parse(body);
            if (!data) {
                res.status(401).json({
                    success: false
                })
            }
            for (let i = 0; i < data.length; i++) {
                counties[data[i].name] = data[i].isoCode
            }
            res.status(200).json({
                success: true,
                data: counties
            })
            
        });



    } catch (error) {
        res.status(401).json({
            success: false
        })
    }
}

exports.cities_old = async(req, res, next) => {
    try {
        const countyCode = req.body.countyCode;
        const options = {
            method: 'GET',
            url: `https://wft-geo-db.p.rapidapi.com/v1/geo/countries/SE/regions/${countyCode}/cities`,
            headers: {
            'x-rapidapi-key': '6f882fb5b5mshb29ecdaca69481fp111c6ajsn4577c25cf140',
            'x-rapidapi-host': 'wft-geo-db.p.rapidapi.com',
            useQueryString: true
            }
        };
        let cities = []
        request(options, function (error, response, body) {
            // if (error) throw new Error(error);
            if (error) {
                res.status(401).json({
                    success: false
                })
            }
            const { data } = JSON.parse(body);
            if (!data) {
                res.status(401).json({
                    success: false
                })
            }
            for (let i = 0; i < data.length; i++) {
                cities.push(data[i].name)
            }
            res.status(200).json({
                success: true,
                data: cities
            })
        });


    } catch (error) {
        res.status(401).json({
            success: false
        })
    }
}