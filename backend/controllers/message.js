const ErrorHandler = require("../utils/errorHandler.js");
const messageModel = require("../models/message.js");
const { getUser } = require("../utils/index.js");

    /**
     * @description register user
     * @param route POST /api/v1/ads/add
     * @param access PRIVATE
     */


exports.sendMessage = async(req, res, next) => {
    try {
        const { recipient, message } = req.body;
        const user = await getUser(req)

            await messageModel.create({
                user: user.name,
                recipient: recipient,
                message: message,
                read: false,
            });
    
            res.status(200).json({
                sucess: true,
                message: "successfully added ad",
            });

    } catch (error) {
        next(error);
    }
};

exports.getConversations = async(req, res, next) => {
    try {
        const user = await getUser(req)
        const conversations = await messageModel.find({
            $or:[{user: user.name},{recipient: user.name}]
            })
        const tempConv =  []
        for (let i = 0; i < conversations.length; i++ ) {
            let name = conversations[i].user
            let read = conversations[i].read
            
            if (user.name == conversations[i].user) {
                name = conversations[i].recipient
                read = true
            }
            if (tempConv.filter(e => e.name === name).length < 1) {
                tempConv.push({name: name, date: conversations[i].createdAt, read: read })
            }
            for (let n = 0; n < tempConv.length; n++ ) {
                if (tempConv[n].name == name && tempConv[n].date < conversations[i].createdAt) {
                    tempConv[n] = {name: name, date: conversations[i].createdAt, read: read}

                }
            }   
        }
        
        
        res.status(200).json({
            sucess: true,
            data: { conversations: tempConv.reverse() }
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({
            sucess: false,
            error: error
        });
    }
};

exports.getMessages = async(req, res, next) => {
    try {
        const user = await getUser(req)
        const { recipient } = req.body;
        const sent = await messageModel.find({
            $and:[{user: user.name},{recipient: recipient}]
            })
        const recieved = await messageModel.find({
                $and:[{user: recipient},{recipient: user.name}]
                })
        await messageModel.updateMany({
                $and:[{user: recipient},{recipient: user.name}]
                }, {read: true})
       
        const sentMessages = sent.map((message) => {
            let name;
            if (message.name != user.name) {
                name = message.user
            }
            else {
                name = message.recipient
            }
            return {
                name: name,
                message: message.message,
                date: message.createdAt
            }
        })
        const recievedMessages = recieved.map((message) => {
            let name;
            if (message.recipient != user.name) {
                name = message.recipient
            }
            else {
                name = message.user
            }
            return {
                name: name,
                message: message.message,
                date: message.createdAt
            }
        })
        
        const messages = (sentMessages.concat(recievedMessages)).sort((a, b) => {
            return a.date - b.date
        })
        const uniqueMessagesList = Array.from(new Set(messages))

        res.status(200).json({
            sucess: true,
            data: { messages: uniqueMessagesList }
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({
            sucess: false,
            error: error
        });
    }
};