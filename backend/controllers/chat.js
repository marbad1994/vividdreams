const ErrorHandler = require("../utils/errorHandler.js");
const roomModel = require("../models/room.js");
const chatModel = require("../models/chat.js");
const userModel = require("../models/auth.js");
const crypto = require("crypto");
const { getUser } = require("../utils");

/**
 * @description register user
 * @param route POST /api/v1/chat/new-chat
 * @param access PRIVATE
 */

class Member {
    constructor(user, connected) {
        this.name = user.name;
        this.connected = connected;
        this.connectionId;
    }
}

class Members {
    constructor() {
        this.members = {};
    }
    addMember(username, connected, key) {
        const member = new Member(username, connected)
        this.members[key] = member;
    }
}


newChat = async(req, res, next) => {
    try {
        const { user, memberTwo } = req.body;
        if (!user || !memberTwo) {
            return
        }
        // const roomId = crypto.randomBytes(20).toString("hex");

        const room = await roomModel.create({
            // roomId: roomId,
            members: [{
                    name: user.name,
                    connected: true
                },
                {
                    name: memberTwo,
                    connected: false
                }
            ]
        });

        res.status(200).json({
            sucess: true,
            data: { user: user, roomId: room._id }
        });
    } catch (error) {
        next(error);
    }
};

/**db.coll.find({services: {$elemMatch : {credits: {$lt: 50}}}});

 * @description check myself
 * @param route GET /api/v1/auth/me
 * @param access PRIVATE
 */

exports.getUserFromConnectionId = async(roomname, username) => {
    try {
        const room = await roomModel
        .findOne({
            _id: roomname,
            members: {
                $elemMatch: {
                    name: username
                }
            }
        })
        // console.log("C_ID: ", connectionId)
        // console.log("2", room)
        for (let key in room.members) {
            if (room.members[key].name == username) {
                return {roomId: room._id, user: room.members[key]}
            }
        }
        return
    } catch (error) {
        return error
    }

}

exports.getConnectionId = async(username, roomId) => {
    try {
        const room = await roomModel
            .findOne({
                _id: roomId,
                members: {
                    $elemMatch: {
                        name: username
                    }
                }
            })
            for (let key in room.members) {
                if (room.members[key].name == username) {
                    return room.members[key].connectionId
                }
            }
    } catch (error) {
        return error
    }
}


exports.updateConnection = async(username, roomId, connected, connectionId) => {
    try {
        let query = {
             _id: roomId,
          
        }

        const room = await roomModel
            .findOne(query)

        for (let key in room.members) {
            if (room.members[key].name == username) {
                room.members[key].connected = connected
                room.members[key].connectionId = connectionId
                break
            }
        }

        roomModel.findOneAndUpdate(query, room, { useFindAndModify: false }, function(err, doc) {
            if (err) return res.send(500, { error: err });
        });

  

    } catch (error) {
        return error
    }
}

// exports.delete = async() => {
//     console.log(await chatModel.deleteMany({
//     }))


// }

/**
 * @description save chat
 * @param route POST /api/v1/chat/save-chat
 * @param access PRIVATE
 */


exports.saveChat = async(req, res, next) => {
    try {
        const roomname = req.body.roomname;
        const chat = req.body.chat;
        let query = {
            roomname: roomname
        }
        let updateChat = {
            roomname: roomname,
            chat: chat
        }
        const chats = await chatModel.findOne(
            query
        )

        if (!chats) {
            await chatModel.create(updateChat)
        } else {
            if (chats.chat.length > chat.length) {
                throw "History longer than update"
            }
            await chatModel.findOneAndUpdate(query, updateChat, { useFindAndModify: false })

        }
        res.status(200).json({
            success: true,
            data: "Created"
        })


    } catch (error) {
        next(error);
    }
}


exports.getChat = async(req, res, next) => {
    try {
        // this.delete()
        const user = await getUser(req)
        console.log("USER", user.name)
        const friend = req.body.friend
        console.log("FRIEND", friend)
        const rooms = await roomModel.find({})

        let query = {

            $and: [{
                    members: {
                        $elemMatch: {
                            name: user.name
                        }
                    }
                },
                {
                    members: {
                        $elemMatch: {
                            name: friend
                        }
                    }
                }
            ]

        }
        const room = await roomModel
            .findOne(query)


        if (room) {
            for (let key in room.members) {
                if (room.members[key].name == user.name) {
                    room.members[key].connected = true
                    break
                }
            }
            let chatHistory = await chatModel.findOne({
                roomname: room._id
            })
            let chat;
            if (chatHistory === null) {
                chat = []
            } else {
                chat = chatHistory.chat
            }
            roomModel.findOneAndUpdate(query, room, { useFindAndModify: false }, function(err, doc) {
                if (err) return res.send(500, { error: err });
                res.status(200).json({
                    sucess: true,
                    data: { roomId: room._id, user: user, chat: chat },
                });
            });



        } else {
            req.body.user = user
            req.body.memberTwo = friend
            newChat(req, res, next)
        }


    } catch (error) {
        next(error);
    }
};