const profileModel = require("../models/profile.js")
const profilePictureModel = require("../models/profilePicture.js");
const friendModel = require("../models/friends.js");

exports.getProfile = async(req, res, next) => {
    try {
        const username = req.body.username;
        

        let profile = await profileModel.findOne({
            username: username
        })

        const query = {
            username: username
        }
        let profilePicture = await profilePictureModel.findOne({
            query
        })
        if (profilePicture === null) {
            profilePicture = {profilePicture: null}
        }
        const friends = await friendModel.find({
            user: username
        })
        .select("+friend");
          const friendList = friends.map( (value) => {
            
            return (
                value.friend
            )
          })
          if (!friendList.includes(req.user.name) && req.user.name !== username) {
              profile.images = profile.images.filter((image) => image.availableFor == "all")

          }

        res.status(200).json({
            success: true,
            data: profile,
            profilePicture: profilePicture.profilePicture
        });
    } catch (error) {
        res.status(500).json({
            success: true,
            data: error,
        });
    }
}

exports.editProfile = async(req, res, next) => {
    try {
        const username = req.body.username;
        const query = {username: username}
        const emitedProfile = req.body.profile;
        if (req.user.name == username) {
            profileModel.findOneAndUpdate(query, emitedProfile, { useFindAndModify: false }, function(err, doc) {
                if (err) return res.send(500, { error: err });
                res.status(200).json({
                    success: true,
                    data: doc
                })
            });
        }

    } catch (error) {
        console.log(error)
        next(error)
    }
}