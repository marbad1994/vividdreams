const ErrorHandler = require("../utils/errorHandler.js");
const adModel = require("../models/ads.js");
const userModel = require("../models/auth.js");
const { getUser } = require("../utils/index.js");
const { faAssistiveListeningSystems } = require("@fortawesome/free-solid-svg-icons");
const profileModel = require("../models/profile.js")

    /**
     * @description register user
     * @param route POST /api/v1/ads/add
     * @param access PRIVATE
     */


exports.adExists = async(name, ad) => {
    try {
        const ads = await adModel.find({
            user: name,
            ad: ad
        })
        console.log(ads.length)

        if (ads.length < 1) {
            return false
        }
        return true
    
       
    } catch (error) {
        next(error);
    }
}

exports.addAd = async(req, res, next) => {
    try {
        const { ad } = req.body;
        const user = await getUser(req)

            await adModel.create({
                user: user.name,
                city: ad.city,
                county: ad.county,
                subject: ad.subject,
                adType: ad.adType,
                description: ad.description,
                ageTo: ad.ageTo,
                ageFrom: ad.ageFrom,
                visited: 0,
                responses: 0,
            });
    
            res.status(200).json({
                sucess: true,
                message: "successfully added ad",
            });

    } catch (error) {
        next(error);
    }
};

exports.getAds = async(req, res, next) => {

    try {
        const ads = await adModel.find({
                
            })
            ;
        const adList = []
        for (let i = 0; i < ads.length; i++) {
            const ad = ads[i]
            const { info } = await profileModel.findOne({username: ad.user})
            tempAd = {}
            tempAd.id = ad._id
            tempAd.sexuality = info.sexuality
            tempAd.age = info.age
            tempAd.user = ad.user
            tempAd.city = ad.city
            tempAd.county = ad.county
            tempAd.subject = ad.subject
            tempAd.adType = ad.adType
            tempAd.description = ad.description
            tempAd.ageTo = ad.ageTo
            tempAd.ageFrom = ad.ageFrom
            tempAd.visited = ad.visited
            tempAd.responses = ad.responses
            tempAd.createdAt = ad.createdAt

            adList.push(tempAd)
        }
        const uniqueAdList = Array.from(new Set(adList)).reverse()

        res.status(200).json({
            sucess: true,
            data: { ads: uniqueAdList }
        });
    } catch (error) {
        res.status(500).json({
            sucess: false,
            error: error
        });
    }
};

exports.deleteAd = async(req, res, next) => {
    

    try {
        const { id } = req.body;
        const user = await getUser(req)
        await adModel.deleteOne({_id: id, username: user.name});
        
        res.status(200).json({
            sucess: true,
        });
    } catch (error) {
        res.status(500).json({
            sucess: false,
            error: error
        });
    }
};

exports.getAd = async(req, res, next) => {
    const { id } = req.body;
    try {
        const ad = await adModel.findOne({_id: id});
        const { info } = await profileModel.findOne({username: ad.user})
            tempAd = {}
            tempAd.id = ad._id
            tempAd.sexuality = info.sexuality
            tempAd.age = info.age
            tempAd.user = ad.user
            tempAd.city = ad.city
            tempAd.county = ad.county
            tempAd.subject = ad.subject
            tempAd.adType = ad.adType
            tempAd.description = ad.description
            tempAd.ageTo = ad.ageTo
            tempAd.ageFrom = ad.ageFrom
            tempAd.visited = ad.visited
            tempAd.responses = ad.responses
            tempAd.createdAt = ad.createdAt

        res.status(200).json({
            sucess: true,
            data: { ad: tempAd }
        });
    } catch (error) {
        res.status(500).json({
            sucess: false,
            error: error
        });
    }
};

exports.getUsersAds = async(req, res, next) => {
    try {
        const { username } = req.body;
        const ads = await adModel.find({
                user: username
            })
        
        const adList = []
        for (let i = 0; i < ads.length; i++) {
            const ad = ads[i]
            const { info } = await profileModel.findOne({username: ad.user})
            tempAd = {}
            tempAd.id = ad._id
            tempAd.sexuality = info.sexuality
            tempAd.age = info.age
            tempAd.user = ad.user
            tempAd.city = ad.city
            tempAd.county = ad.county
            tempAd.subject = ad.subject
            tempAd.adType = ad.adType
            tempAd.description = ad.description
            tempAd.ageTo = ad.ageTo
            tempAd.ageFrom = ad.ageFrom
            tempAd.visited = ad.visited
            tempAd.responses = ad.responses
            tempAd.createdAt = ad.createdAt

            adList.push(tempAd)
        }
        const uniqueAdList = Array.from(new Set(adList)).reverse()

        res.status(200).json({
            sucess: true,
            data: { ads: uniqueAdList }
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({
            sucess: false,
            error: error
        });
    }
};