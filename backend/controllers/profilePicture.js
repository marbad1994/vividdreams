const request = require("request");
const profilePictureModel = require("../models/profilePicture.js");
const profileModel = require("../models/profile.js");

const { getUser } = require("../utils/index.js");

exports.likeImage = async (req, res, next) => {
  try {
    const user = await getUser(req);
    const profileName = req.body.profileName
    const id = req.body.id
    const query = {
      username: profileName,
    };

    const {images} = await profileModel.findOne(query);
    const tmp = images.map((image) => {
      if (image._id == id) {
        if (image.likes.includes(user.name)) {
          const index = image.likes.indexOf(user.name)
          image.likes.splice(index, 1)
        } else {
          image.likes.push(user.name)
        }

      }
      return image
    })
   

    await profileModel.findOneAndUpdate(
      query,
      {images: tmp},
    )
    res.status(200).json({
      sucess: true,
      data: { msg: "Profile picture successfully uploaded" },
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.deleteImageInGallery = async (req, res, next) => {
  try {
    const user = await getUser(req);

    const id = req.body.id


    const query = {
      username: user.name,
    };

    const images = await profileModel.findOne(
      query,
    ).select("+images");

    for (let i = 0; i < images.images.length; i++) {
      if (images.images[i]._id == id) {
        images.images.splice(i, 1)
      }
    }

    await profileModel.findOneAndUpdate(
      query,
      {images: images.images},
    )
    res.status(200).json({
      sucess: true,
      data: { msg: "Profile picture successfully uploaded" },
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.editImageInGallery = async (req, res, next) => {
  try {
    const user = await getUser(req);

    const id = req.body.id
    const title = req.body.title
    const description = req.body.description
    const availableFor = req.body.availableFor
    const query = {
      username: user.name,
    };

    const images = await profileModel.findOne(
      query,
    ).select("+images");

    for (let i = 0; i < images.images.length; i++) {
      if (images.images[i]._id == id) {
        images.images[i].title = title
        images.images[i].description = description
        images.images[i].availableFor = availableFor
      }
    }

    await profileModel.findOneAndUpdate(
      query,
      {images: images.images},
    )
    res.status(200).json({
      sucess: true,
      data: { msg: "Profile picture successfully uploaded" },
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.uploadImageToGallery = async (req, res, next) => {
  try {
    const user = await getUser(req);

    const file = req.files.file;
    const filename = file.name;

    let Client = require("ssh2-sftp-client");
    let sftp = new Client();
    sftp
      .connect({
        host: "ssh.appsight.se",
        port: "22",
        username: "appsight.se",
        password: "Cobcob1994",
      })
      .then(() => {
        return sftp.exists(`/customers/f/d/4/appsight.se/httpd.www/filestorage/vivid-dreams/${user.name}`)
        }
      )
        .then((data) => {
          if (!data) {
            return sftp.mkdir(`/customers/f/d/4/appsight.se/httpd.www/filestorage/vivid-dreams/${user.name}`);
          }
          return
        }
        )
      .then(() => {
        
        return sftp.put(
          file.data,
          `/customers/f/d/4/appsight.se/httpd.www/filestorage/vivid-dreams/${user.name}/${filename}`,
          {
            writeStreamOptions: {
              flags: 'w',  // w - write and a - append
              encoding: null, // use null for binary files
              mode: 0o666, // mode to use for created file (rwx)
          }}
        );
      })
      .then((data) => {
        console.log(data, "the data info");
      })
      .catch((err) => {
        console.log(err, "catch error");
      });
    const imgURL = `https://appsight.se/filestorage/vivid-dreams/${user.name}/${filename}`;
    const imageObj = {
      title: req.body.title,
      description: req.body.description,
      likes: [],
      imgPath: imgURL,
      availableFor: req.body.availableFor
    }
    const query = {
      username: user.name,
    };

    await profileModel.findOneAndUpdate(
      query,
      {$push: {
        images: imageObj
      }}
    );
    res.status(200).json({
      sucess: true,
      data: { msg: "Profile picture successfully uploaded" },
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};
exports.getImages = async (req, res, next) => {
  try {
    const user = req.params.name
    const query = {
      username: user,
    };

  
    const images = await profileModel.findOne({
      query,
    }).select("+images");
    res.status(200).json({
      success: true,
      data: images,
    });
  } catch (error) {
    next(error);
  }
};
exports.uploadProfilePicture = async (req, res, next) => {
  try {
    const user = await getUser(req);

    const newpath = __dirname + "/files/";
    const file = req.files.file;
    const filename = file.name;

    file.mv(`${newpath}${filename}`);
    const newFilename = `${user.name}.${filename.split(".").slice(-1)}`;
    let Client = require("ssh2-sftp-client");
    let sftp = new Client();

    sftp
      .connect({
        host: "ssh.appsight.se",
        port: "22",
        username: "appsight.se",
        password: "Cobcob1994",
      })
      .then(() => {
        return sftp.fastPut(
          `${newpath}${filename}`,
          `/customers/f/d/4/appsight.se/httpd.www/filestorage/vivid-dreams/${newFilename}`
        );
      })
      .then((data) => {
        console.log(data, "the data info");
      })
      .catch((err) => {
        console.log(err, "catch error");
      });
    const imgURL = `https://appsight.se/filestorage/vivid-dreams/${newFilename}`;
    const query = {
      username: user.name,
    };

    await profileModel.findOneAndUpdate(
      query,
      { profilePicture: imgURL },
      { upsert: true }
    );
    console.log(imgURL)
    res.status(200).json({
      success: true,
      data: imgURL,
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.getProfilePicture = async (req, res, next) => {
  try {
    const user = req.params.name
    const query = {
      username: user,
    };
    const profilePicture = await profileModel.findOne(
      query,
    );
    res.status(200).json({
      success: true,
      data: profilePicture.profilePicture,
    });
  } catch (error) {
    next(error);
  }
};
