const ErrorHandler = require("../utils/errorHandler.js");
const userModel = require("../models/auth.js");
const chatModel = require("../models/chat.js");
const profileModel = require("../models/profile.js");
const request = require('request');
const { getUser } = require("../utils/index.js");

    /**
     * @description register user
     * @param route POST /api/v1/auth/register
     * @param access PRIVATE
     */


exports.registerUser = async(req, res, next) => {
    try {
        const { name, email, password, role, county, city, sexuality, gender, age } = req.body;
        if (age < 18) {
            res.status(403).json({
                success: false
            })
        }
        const user = await userModel.create({
            name,
            email,
            password,
        });

        await profileModel.create({
            userId: user.id,
            username: name,
            county: county,
            city: city,
            about: "",
            lookingFor: "",
            info: {
                lastLoggedIn: Date.now().toString(),
                gender: gender,
                sexuality: sexuality,
                etnicity: "",
                height: null,
                weight: null,
                age: age,
                bodyType: ""
            },
            interests: []
        })

        const token = user.getSignedJwtToken();

        res.status(200).json({
            success: true,
            token,
        });
    } catch (error) {
        next(error);
    }
};

/**
 * @description login user
 * @param route POST /api/v1/auth/login
 * @param access PRIVATE
 */
exports.loginUser = async(req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
    try {
        const { name, password } = req.body;

        //validate name and password
        if (!name || !password) {
            return next(new ErrorHandler("Please provide name password", 400));
        }

        //check for user
        const user = await userModel
            .findOne({
                name,
            })
            .select("+password");
        console.log(user, user.resetPasswordExpire)
        if (!user) {
            return next(new ErrorHandler("Invalid credentials", 400));
        }

        //check password
        const isMatch = await user.matchpasswords(password);
        if (!isMatch) {
            return next(new ErrorHandler("Invalid credentials", 400));
        }

        //NOTE: static are called on model so , it will called upon userModel
        //methods are called on the actual user data , so it will call on 'user'

        const token = user.getSignedJwtToken();
        const profile = await profileModel.findOne({username: user.name})
        
        const expiresAt = new Date(Date.now() + (19 * 60 * 1000) );
        await userModel.updateOne({name}, {$set: {expiresAt: expiresAt}})
        res.status(200).json({
            success: true,
            token,
            expiresAt,
            profile,
        });
    } catch (error) {
        next(error);
    }
};

/**
 * @description check myself
 * @param route GET /api/v1/auth/me
 * @param access PRIVATE
 */
exports.getMe = async(req, res, next) => {
    try {
        const user = await userModel.findById(req.user.id);

        res.status(200).json({
            sucess: true,
            data: user,
        });
    } catch (error) {
        next(error);
    }
};

exports.logout = async(req, res, next) => {
    try {
        const user = await getUser(req)

        await userModel.updateOne({name: user.name}, {$set: {expiresAt: null}})

        res.status(200).json({
            sucess: true,
        });
    } catch (error) {
        next(error);
    }
};

exports.refreshToken = async(req, res, next) => {
    try {
        const user = await getUser(req)
        const expiresAt = new Date(Date.now() + (19 * 60 * 1000) );

        await userModel.updateOne({name: user.name}, {$set: {expiresAt: expiresAt}})

        res.status(200).json({
            sucess: true,
        });
    } catch (error) {
        console.log(error)
        next(error);
    }
};