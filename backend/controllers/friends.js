const ErrorHandler = require("../utils/errorHandler.js");
const friendModel = require("../models/friends.js");
const userModel = require("../models/auth.js");
const profileModel = require("../models/profile.js");

const { getUser } = require("../utils/index.js");
const { faAssistiveListeningSystems } = require("@fortawesome/free-solid-svg-icons");
    /**
     * @description register user
     * @param route POST /api/v1/friends/add
     * @param access PRIVATE
     */


exports.friendExists = async(name, friend) => {
    try {
        const friends = await friendModel.find({
            user: name,
            friend: friend
        })
        console.log(friends.length)

        if (friends.length < 1) {
            return false
        }
        return true
    
       
    } catch (error) {
        next(error);
    }
}

exports.addFriend = async(req, res, next) => {
    try {
        const { friend } = req.body;
        const user = await getUser(req)

        if (await this.friendExists(user.name, friend)) {
            res.status(403).json({
                sucess: false,
                message: "Friend exists!"
            })
        } else if (user.name == friend) {
            res.status(403).json({
                sucess: false,
                message: "Can't add yourself as a friend"
            })
        } else {
            await friendModel.create({
                user: user.name,
                friend: friend
            });
    
            res.status(200).json({
                sucess: true,
                message: "successfully added friend",
            });
        }

    } catch (error) {
        next(error);
    }
};

exports.getFriends = async(req, res, next) => {
    try {
        const user = await getUser(req)
        const friends = await friendModel.find({
                user: user.name
            })
            .select("+friend");
        const friendList = friends.map( (value) => {
            
            return (
                value.friend
            )
        })
        const uniqueFriendList = Array.from(new Set(friendList))
        const users = await userModel.find({})
        const friendProfile = await profileModel.find()
        const onlineFriends = users.map((friend) =>  {
            if (uniqueFriendList.includes(friend.name)) {
                let profilePicture;
                let lastLoggedIn;
                for (let i = 0; i < friendProfile.length; i++) {
                    if (friendProfile[i].username == friend.name) {
                        profilePicture = friendProfile[i].profilePicture
                        lastLoggedIn = friendProfile[i].info.lastLoggedIn
                        break
                    } 
                }
                return {profilePicture: profilePicture, lastLoggedIn: lastLoggedIn, name: friend.name, online: friend.expiresAt ? friend.expiresAt > new Date() : false}
                }
        })
        console.log(onlineFriends)

        res.status(200).json({
            sucess: true,
            data: { friends: onlineFriends.filter(item => item !== undefined) }
        });
    } catch (error) {
        res.status(500).json({
            sucess: false,
            error: error
        });
    }
};