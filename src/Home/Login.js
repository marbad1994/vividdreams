import React, { useState } from "react";
import "./home.scss";
import services from '../services';
import { login, isLogin } from "../utils"
import { Link } from "react-router-dom";

const { AuthService } = services;



function Login (props) {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const history = props.history;
  // const { setProfile, setInfo, setUsername } = props.context;

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      const { token, expiresAt, profile } = await AuthService.login(name, password);
      await login(token, name, expiresAt, profile);
      if (isLogin()) {

        history.push("/")

        
      }

    } catch (ex) {
     console.log(ex)
    }

  }


  return (
    <div style={{minHeight: "calc(100vh - 80px)"}}>
    <div className="homepage">
      <h1 style={{color: "#CCCCCC"}}>Logga in</h1>
      <form  onSubmit={handleSubmit}>
      <input
        placeholder="Användarnamn"
        value={name}
        onChange={(e) => setName(e.target.value)}
        style={{color: "#CCCCCC", marginBottom: 30}}
        color="#CCCCCC"
      ></input>
      <input
        placeholder="Lösenord"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        style={{color: "#CCCCCC", marginBottom: 30}}
        color="#CCCCCC"
      ></input>
      {/* <Link to={`/chat/${roomname}/${username}`}> */}
        <button value="submit" style={{color: "#CCCCCC", width: "80%"}} type="submit">Logga in</button>
        </form>

      {/* </Link> */}
      <Link style={{textAlign: "left", marginLeft: 35}} to="/register">Registrera användare</Link>
    </div>
    </div>
  );
}

export default Login;
