import React, { useState, useEffect } from "react";
import "./home.scss";
import services from '../services';
import { login, isLogin } from "../utils"
import { Link } from "react-router-dom";

const { AuthService } = services;


function Register (props) {
  const [name, setusername] = useState("");
  const [password, setPassword] = useState("");
  const [secondPassword, setSecondPassword] = useState("");
  const [email, setEmail] = useState("");
  const [counties, setCounties] = useState({})
  const [cities, setCities] = useState([])
  const [county, setCounty] = useState("")
  const [city, setCity] = useState("")
  const [gender, setGender] = useState("")
  const [sexuality, setSexuality] = useState("")
  const [age, setAge] = useState(null)

  useEffect(() => {
    async function getGeo() {
      const countyData = await AuthService.getCounties()
      setCounties(countyData.data)
    }
      getGeo()

    },[]);


  const getCities = async (county) => {
    setCounty(county)
    const countyCode = counties[county];
    const cityData = await AuthService.getCities(countyCode)
    setCities(cityData.data)
  }

  const registerHandler = async () => {
    try {
      if (password != secondPassword) {
        throw "Passwords doesn't match";
      }
      const { token } = await AuthService.register(name, password, email, county, city, sexuality, gender, age);
      console.log(token)
      login(token, name);
      // if ( isLogin()) {
      //       props.history.push("/profile")
      // }
      
    } catch (ex) {
     console.log(ex)
    }


  }

  return (
    <div className="register">
      <h1 style={{color: "#CCCCCC"}}>Registrering</h1>
      <form>
      <input
        placeholder="Användarnamn"
        value={name}
        onChange={(e) => setusername(e.target.value)}
        color="#CCCCCC"
      ></input>
      <input
        placeholder="Lösenord"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        color="#CCCCCC"
      ></input>
        <input
        placeholder="Upprepa lösenord"
        type="password"
        value={secondPassword}
        onChange={(e) => setSecondPassword(e.target.value)}
        color="#CCCCCC"
      ></input>
        <input
        placeholder="email"
        type="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        color="#CCCCCC"
      ></input>
       <input
        placeholder="sexuality"
        type="text"
        value={sexuality}
        onChange={(e) => setSexuality(e.target.value)}
        color="#CCCCCC"
      ></input>
       <input
        placeholder="gender"
        type="text"
        value={gender}
        onChange={(e) => setGender(e.target.value)}
        color="#CCCCCC"
      ></input>
       <input
        placeholder="age"
        type="number"
        value={age}
        onChange={(e) => setAge(e.target.value)}
        color="#CCCCCC"
      ></input>
      <div>
          <select id="counties" name="counties" onChange={(e) => getCities(e.target.value)}>
          <option value="" selected disabled hidden>Län: </option>
            {Object.keys(counties).map((key, index)=>(
              <option value={key} key={index}>{key}</option>
            ))}
          </select>
          </div>
          <div>
          <select id="citites" name="cities" onChange={(e) => setCity(e.target.value)}>
          <option value="" selected disabled hidden>Stad: </option>
            {cities.map((key, index)=>(
              <option value={key} index={index}>{key}</option>
            ))}
          </select>
          </div>
      {/* <Link to={`/chat/${roomname}/${username}`}> */}
        <button style={{color: "#CCCCCC", width: "80%"}} type="submit" onClick={registerHandler}>Registrera</button>
        </form>

      {/* </Link> */}
      <Link style={{textAlign: "left", marginLeft: 35}} to="/login">Logga in</Link>
    </div>
  );
}

export default Register;
