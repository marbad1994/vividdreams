import React, { Component } from 'react'

const Context = React.createContext()

class Provider extends Component {
  // Context state
  state = {
    profile: {},
  }

  // Method to update state
  setProfile = (profile) => {
    this.setState((prevState) => ({ profile }))
  }

  render() {
    const { children } = this.props
    const { profile } = this.state
    const { setProfile } = this

    return (
      <Context.Provider
        value={{
          profile,
          setProfile,
        }}
      >
        {children}
      </Context.Provider>
    )
  }
}

export default Context

export { Provider }