import React, { useState, useEffect, useRef } from "react";
import "./chat.scss";
import { DoDecrypt, DoEncrypt } from "../aes.js";
import { useDispatch } from "react-redux";
import { process } from "../store/action/index";
import services from '../services';
import { getUsername } from "../utils"

const { AuthService } = services;


function Chat({ username, roomname, socket, user, url, importedChatHistory, friend }) {
  const [text, setText] = useState("");
  const [chatHistory, setChatHistory] = useState({[`${roomname}`]: importedChatHistory})
  const [messages, setMessages] = useState([]);
  const [userName, setUser] = useState(username)
  const [path, setPath] = useState(url)
  const [roomId, setRoomId] = useState(roomname)
  const dispatch = useDispatch();

  const dispatchProcess = (encrypt, msg, cipher) => {
    dispatch(process(encrypt, msg, cipher));
  };


  useEffect(() => {
   async function saveChat() {
  if (chatHistory[`${roomId}`] !== undefined &&  chatHistory[`${roomId}`].length > 0 ) {
    
     await AuthService.saveChat(roomId, chatHistory[`${roomId}`])
   }
 }
   saveChat()

   },[chatHistory]);




  useEffect(() => {
    if (url != path) {
      let chatTemp = chatHistory;
      
      if (chatTemp[`${roomId}`] == undefined) {
        chatTemp[`${roomId}`] = []
        setChatHistory({...chatTemp})
      }

      let roomid = url.split("/")[1]


      setRoomId(roomid)
      setPath(url)
    }
  }, [url])

const decrypt = (text, username) => {
  const ans = DoDecrypt(text, username);
  dispatchProcess(false, ans, text);
  return ans;
}

  useEffect(() => {
    socket.on("message", (data) => {
      let chatTemp = chatHistory;
      if (chatTemp[`${data.roomname}`] == undefined) {
        chatTemp[`${data.roomname}`] = []
      }
      let timestamp = new Date()
      chatTemp[`${data.roomname}`].push({
        userId: data.userId,
        username: data.username,
        text: data.text,
        timestamp: timestamp
      });
      setChatHistory({...chatTemp})
    });
  }, [socket, userName]);

  const sendData = (roomname) => {
    if (text !== "") {
      const ans = DoEncrypt(text);
      socket.emit("chat", {text: ans, roomname: roomname, username: getUsername(), chat: chatHistory[`${roomname}`]});
      setText("");
    }
  };
  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(scrollToBottom, [chatHistory]);

  const capitalize = (s) =>
  {
    if (!s) {
      return ""
    }
      return s[0].toUpperCase() + s.slice(1);
  }

  return (
    <div className="chat">
      <div className="user-name">
        <h2>
          Chattar med: {capitalize(friend)} 
        </h2>
      </div>
      <div className="chat-message">
        {((chatHistory[`${roomname}`] !== undefined) || chatHistory[`${roomname}`] !== [] ) && chatHistory[`${roomname}`].map((i, index) => {
          if (i.username === getUsername()) {
            return (
              <div key={index} className="message mess-right">
                <p>{decrypt(i.text, i.username)}</p>
                <span>{i.username} </span>
                <span style={{fontSize: 12}}>{i.timestamp.toString().slice(0,-40)}</span>              </div>
            );
          } else {
            return (
              <div key={index} className="message mess-left">
                <p>{decrypt(i.text, i.username)}</p>
                <span>{i.username} </span>
                <span style={{fontSize: 12}}>{i.timestamp.toString().slice(0,-40)}</span>
              </div>
            );
          }
        })}
        <div ref={messagesEndRef} />
      </div>
      <div className="send">
        <input
          placeholder="Skriv..."
          value={text}
          onChange={(e) => setText(e.target.value)}
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              sendData(roomname);
            }
          }}
        ></input>
        <button onClick={() => sendData(roomname)}>Skicka</button>
      </div>
    </div>
  );
}
export default Chat;
