import React from 'react';
import { useEffect, useState } from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import { Link } from "react-router-dom";
import Chat from '../chat/chat.js';
import services from '../services';
import { getUsername } from "../utils"

const { AuthService } = services;
class Messages extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      avatar: "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png",
      friends: [],
      chat: false,
      chatHistory: [],
      user: {},
      username: "",
      friend: ""
    }
  }


// const Messages = (props) => {
//   const avatar = "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png"
//   const [friends, setFriends] = useState([])
//   const [chat, setChat] = useState(false)
//   const [chatHistory, setChatHistory] = useState([])
//   const [user, setUser] = useState(getUsername())
   async componentDidMount() {
    //  socket.on('chat', "On")
      const friendData = await AuthService.getFriends()
      let username = getUsername()
      
      const state = this.props.history.location.state;
      let chat = false;
      let chatHistory = [];
        if (state !== undefined){
        chat = state.chat;
        chatHistory = state.chatHistory;
        username = state.user;
      }
      this.setState({friends: friendData.data.friends.map((friend) => {return friend.name}), username: username, chat: chat, chatHistory: chatHistory})
    }

    async sendData (friend) {
        if (friend !== "") {
          this.props.history.push("/messages")
            const roomInfo = await AuthService.getChat(friend)
            let user = roomInfo.data.user
            let roomId = roomInfo.data.roomId
            let chatHistory = roomInfo.data.chat
            if (chatHistory.length > 2) {
            chatHistory.sort((a,b)=>a.timestamp>b.timestamp)
              
            }

            this.props.socket.emit("joinRoom", { user, roomId });
            this.state.chatHistory = chatHistory
            this.state.friend = friend
             this.props.history.push(
               {pathname: `/chat/${roomId}/${friend}`,
               state: {chat: true, user: user.name, chatHistory: chatHistory}})
        } else {
          alert("username and roomname are must!");
        }
      };
render() {
  return (
    <div style={{backgroundColor: "#2D343E", width: "100%", minHeight: 'calc(100vh - 81px)', display: "flex"}}>
      <div style={{width: "100%", maxWidth: 220, marginLeft: 20}}>

        <h2 style={{color: "#b4b6be", marginTop: 22, textAlign: "left", fontWeight: 300, paddingBottom: "1rem", borderBottom: "1px solid rgba(255, 255, 255, 0.1)"}}>Kontakter</h2>
        <ListGroup className="friends-list" style={{overflowY: "scroll", height: "calc(100vh - 175px)", scrollbarWidth: "thin"}}>
            {
                this.state.friends.map((value, index) => {
                  return (
                  <ListGroupItem key={index} tag="button" style={{backgroundColor: "#404450", color: "#b4b6be", fontSize: "1.4rem", fontWeight: 300, textAlign: "left", textTransform: "capitalize"}} action onClick={() => this.sendData(value)} ><img src={this.state.avatar} style={{height: 35, borderRadius: "50%", marginRight: 30, marginLeft: 10}}></img>{value}</ListGroupItem>
                  )

                })
            }
          
        </ListGroup>
        </div>
     {this.props.match.params.roomname &&
     <>     
      <React.Fragment>
      <Chat
        username={this.state.username}
        roomname={this.props.match.params.roomname}
        socket={this.props.socket}
        user={this.state.user}
        url={this.props.location.pathname}
        importedChatHistory={this.state.chatHistory}
        friend={this.state.friend}
      />
  </React.Fragment>
     </>}
      </div>
  )
  }
}

export default Messages


