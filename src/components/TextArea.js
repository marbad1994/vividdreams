import React from 'react';

class TextArea extends React.Component {
    constructor(props) {
      super(props)
      this.value = props.value
    }

    setValue = (event) => {
        console.log(this.props)
        let value = event.target.value;
        this.value = value
        this.props.setInfo(value, this.props.category)
    }

    render() {
    return (
    <textarea
      value={this.value}
      onChange={(e) => this.setValue(e)}
      color="#CCCCCC"
      style={{width: "100%"}}
    ></textarea>
    )
    }
  }

export default TextArea
