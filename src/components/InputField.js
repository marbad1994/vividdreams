import React from 'react';

class InputField extends React.Component {
    constructor(props) {
      super(props)
      this.value = props.value
    }

    setValue = (event) => {
        console.log(this.props)
        let value = event.target.value;
        this.value = value
        this.props.setInfo(value, this.props.category)
    }

    render() {
    return (

    <input
      type={this.props.type}
      value={this.value}
      onChange={(e) => this.setValue(e)}
      color="#CCCCCC"
    ></input>
    )
    }
  }

export default InputField
