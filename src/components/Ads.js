import React, { useEffect, useState } from 'react';
import { Button, Col, Input, Form, Row, FormGroup, Label } from 'reactstrap';

import Ad from './Ad';
import { faPlusSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import services from '../services';

const { AuthService } = services;
const Ads = (props) => {
  const [counties, setCounties] = useState([])
  const [cities, setCities] = useState([])
  const [selectedCounty, setSelectedCounty] = useState("")
  const [selectedCity, setSelectedCity] = useState("")
  const [ads, setAds] = useState([])
  const [ageFromFilter, setAgeFromFilter] = useState(0)
  const [ageToFilter, setAgeToFilter] = useState(0)
  const [searchStringFilter, setSearchStringFilter] = useState("")
  const [adTypeFilter, setAdTypeFilter] = useState([])
  const [newAd, setNewAd] = useState({
    city: selectedCity,
    county: selectedCounty,
    subject: "",
    description: "",
    adType: "",
    ageFrom: 18,
    ageTo: 99
  })

  useEffect(() => {
    const getCounties = async () => {
      const response = await AuthService.getCounties()
      setCounties(response.data)
      await getCities(Object.keys(response.data)[0], response.data)
      const username = props.match.params.username
      let res;
      if (username) {
        res = await AuthService.getUsersAds(username)
      } else {
        res = await AuthService.getAds()
      }
      setAds(res.data.ads)

    }
    getCounties()
  }, [props.match.params.username])

  const getCities = async (county, resCounties) => {
    setSelectedCounty(county)
    const countyCode = counties[county] ? counties[county] : resCounties[county];
    const cityData = await AuthService.getCities(countyCode)
    setCities(cityData.data)
    setSelectedCity(cityData.data[0])
    const ad = {...newAd}
    ad.county = county
    ad.city = cityData.data[0]
    setNewAd(ad)
  } 
  const setValue = (event, key) => {
    event.preventDefault()
    const ad = {...newAd}
    ad[`${key}`] = event.target.value
    setNewAd(ad)
  }
  const publishAd = async (event) => {
    event.preventDefault()
    if (Object.prototype.toString.call(newAd.ageFrom) === '[object String]') {
      newAd.ageFrom = parseInt(newAd.ageFrom)
    }
    if (Object.prototype.toString.call(newAd.ageTo) === '[object String]') {
      newAd.ageTo = parseInt(newAd.ageTo)
    }
    await AuthService.postAd(newAd)
    const { data } = await AuthService.getAds()
    setAds(data.ads)
    setNewAd({})
  }

  const deleteAd = async (id) =>  {
    await AuthService.deleteAd(id)

    setAds(ads.filter(ad => id != ad.id))
  }

  const adSearch = () => {
    setAds(ads.filter(ad => ad.ageTo <= ageToFilter && ad.ageFrom >= ageFromFilter && ad.user.startsWith(searchStringFilter) && selectedCity == ad.city && selectedCounty == ad.county && adTypeFilter.includes(ad.adType)))
  }

  return (
    <div style={{backgroundColor: "rgb(249, 249, 249)", width: "100%", minHeight: 'calc(100vh - 81px)'}}>

               <div className="list-group-item" style={{marginLeft: 20, marginRight: 20, marginTop: 20}}>
                 <div style={{display: "flex"}}>
       <h1 style={{marginTop: "auto", marginBottom: "auto"}}>Annonser</h1>
       <div style={{ marginLeft: "auto" }}>
               <button type="button" className="btn btn-primary transparent-btn" type="button" data-bs-toggle="modal" data-bs-target="#addAdModal" >
               <FontAwesomeIcon  color="#870060" className="icon-btn" icon={faPlusSquare}/>
</button>
<div class="modal fade" style={{top: "25%"}} id="addAdModal" tabindex="-1" aria-labelledby="addAdModalLabel" aria-hidden="true" data-bs-toggle="tooltip" data-bs-placement="top" title="Lägg upp annons">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addAdModalLabel">Lägg in annons</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <Form>
      <Row form>
  <Col md={6}>
  <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="subject" style={{marginRight: "auto"}}>
      Rubrik
    </Label>
    <Input
      id="subject"
      name="subject"
      placeholder="Rubrik"
      type="text"
      onChange={(e) => setValue(e, "subject")}
    />
  </FormGroup>
    </Col>
    <Col md={3}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="geoAreaAdd" style={{marginRight: "auto"}}>
      Län
    </Label>
    <Input
      id="geoAreaAdd"
      name="select"
      type="select"
      onChange={(e) => getCities(e.target.value)}
    >
     {counties !== undefined && Object.keys(counties).map((key, index)=>(
              <option value={key} key={index}>{key}</option>
            ))}
    </Input>
  </FormGroup>
    </Col>
    <Col md={3}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="citiesAdd" style={{marginRight: "auto"}}>
      Städer
    </Label>
    <Input
      id="citiesAdd"
      name="select"
      type="select"
      onChange={(e) => setSelectedCity(e.target.value)}
    >
      { cities != null && cities.map((key, index)=>(
              <option value={key} key={index}>{key}</option>
            ))}
     
    </Input>
  </FormGroup>
    </Col>
    </Row>
        <Row form>
  <Col md={12}>
  <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="description" style={{marginRight: "auto"}}>
      Beskrivning
    </Label>
    <Input
      id="description"
      name="text"
      type="textarea"
      onChange={(e) => setValue(e, "description")}

    />
  </FormGroup>
  </Col>
</Row>
  <Row form>
    <Col md={6}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="adTypeAdd" style={{marginRight: "auto"}}>
      Annonstyp
    </Label>
    <Input
      id="adTypeAdd"
      name="select"
      type="select"
      onChange={(e) => setValue(e, "adType")}

    >
     <option>Kille Söker Tjej</option>
     <option>Tjej Söker Kille</option>
     <option>Kille Söker Kille</option>
     <option>Tjej Söker Tjej</option>
     <option>Tjej Söker Trans</option>
     <option>Kille Söker Trans</option>
     <option>Trans Söker Trans</option>
     <option>Trans Söker Kille</option>

     
    </Input>
  </FormGroup>
 
    </Col>
    <Col md={3}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="ageFromAdd" style={{marginRight: "auto"}}>
      Ålder Från
    </Label>
    <Input
      id="ageFromAdd"
      name="number"
      min={18}
      placeholder={18}
      max={newAd.ageTo}
      type="number"
      onChange={(e) => setValue(e, "ageFrom")}

    />
  </FormGroup>
    </Col>
    <Col md={3}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="ageToAdd" style={{marginRight: "auto"}}>
      Ålder Till
    </Label>
    <Input
      id="ageToAdd"
      name="number"
      placeholder={99}
      type="number"
      min={newAd.ageFrom}
      onChange={(e) => setValue(e, "ageTo")}
    />
  </FormGroup>
    </Col>
  </Row>
  </Form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onClick={(event) => publishAd(event)}>Publicera in annons</button>
      </div>
    </div>
  </div>
</div>
                </div>
                </div>
       </div>
       <div style={{padding: 20}}>
       <div>
       <div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
        Visa filter
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
      <Form>
  <Row form>
    <Col md={6}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="geoArea" style={{marginRight: "auto"}}>
      Län
    </Label>
    <Input
      id="geoArea"
      name="select"
      type="select"
      onChange={(e) => getCities(e.target.value)}
    >
     {counties !== undefined && Object.keys(counties).map((key, index)=>(
              <option value={key} key={index}>{key}</option>
            ))}
    </Input>
  </FormGroup>
 
    </Col>
    <Col md={3}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="ageFrom" style={{marginRight: "auto"}}>
      Ålder Från
    </Label>
    <Input
      min={18}
      id="ageFrom"
      name="number"
      placeholder={18}
      type="number"
      onChange={(e) => setAgeFromFilter(e.target.value)}
    />
  </FormGroup>
    </Col>
    <Col md={3}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="ageTo" style={{marginRight: "auto"}}>
      Ålder Till
    </Label>
    <Input
      id="ageTo"
      name="number"
      min={ageFromFilter}
      placeholder={99}
      type="number"
      onChange={(e) => setAgeToFilter(e.target.value)}
    />
  </FormGroup>
    </Col>
  </Row>
  <Row>
  <Col md={6}>
  <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="cities" style={{marginRight: "auto"}}>
      Städer
    </Label>
    <Input
      id="cities"
      multiple
      name="selectMulti"
      type="select"
      onChange={(event) => setValue(event, "city")}
    >
      { cities != null && cities.map((key, index)=>(
              <option value={key} key={index}>{key}</option>
            ))}
     
    </Input>
  </FormGroup>
    </Col>
    <Col md={6}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="addType" style={{marginRight: "auto"}}>
      Annonstyp
    </Label>
    <Input
      id="addType"
      multiple
      name="selectMulti"
      type="select"
      onChange={(e) => {const tmp = [...adTypeFilter]; tmp.push(e.target.value); setAdTypeFilter(tmp)}}
    >
     <option>Kille Söker Tjej</option>
     <option>Tjej Söker Kille</option>
     <option>Kille Söker Kille</option>
     <option>Tjej Söker Tjej</option>
     <option>Tjej Söker Trans</option>
     <option>Kille Söker Trans</option>
     <option>Trans Söker Trans</option>
     <option>Trans Söker Kille</option>

     
    </Input>
  </FormGroup>
    </Col>
  </Row>
  <Row>
    <Col md={6}>
    <FormGroup>
    <Input
      id="search"
      name="search"
      placeholder="Sök på användarnamn..."
      onChange={(e) => setSearchStringFilter(e.target.value)}
    />
  </FormGroup>
    </Col>
    <Col md={3}>
    <FormGroup
    check
  >
    <Input type="checkbox" />
    <Label check>
      Visa Bara Online
    </Label>
  </FormGroup>
    </Col>
    <Col md={3}>
    <button className='btn btn-primary' onClick={() => adSearch()} style={{width: "100%"}}>
    Sök
  </button>
    </Col>
  </Row>
  </Form>
      </div>
    </div>
  </div>

</div>
</div>

</div>
{ads.map((value, index) => {
  return (
    <Ad history={props.history} ad={value} deleteAd={deleteAd} key={index}/>
  )
})}
       
      </div>
  )
}

export default Ads


