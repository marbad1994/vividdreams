import React from 'react'
import './SideMenu.css';
import { Badge } from 'reactstrap';


const RecentlyVisitAvatar = props => {
    const { avatar, name } = props

  const displayTooltip = (id, value) => {
      document.getElementById(id).style.display = value
  }

  return (
        <div>
            <Badge id={name} style={{display: "none", marginLeft: 10}} className="VisitedBadge" color="secondary">{name}</Badge>
            <img onMouseEnter={e => {
                     displayTooltip(name, "block")
                 }}
                 onMouseLeave={e => {
                  displayTooltip(name, "none")
              }}
                 src={avatar} className="Visited"></img>

      </div>


     
  )
}

export default RecentlyVisitAvatar
