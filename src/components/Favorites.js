import React from 'react';
import { useEffect, useState } from 'react';
import services from '../services';
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
const { AuthService } = services;

const Favorites = () => {
  const [friends, setFriends] = useState([])
  useEffect(() => {
    const getFriends = async () => {
      const response = await AuthService.getFriends()
      console.log(response)
      setFriends(response.data.friends)
    }
    getFriends()
  }, []);

  return (
    <div style={{ backgroundColor: "#fff", width: "100%", minHeight: 'calc(100vh - 81px)' }}>
      <div className="list-group">
        <div className="list-group-item">
          <h1>Favoriter</h1>
        </div>
        {friends.map((friend) => {
          return (
            <a className="list-group-item list-group-item-action" href={`/user/${friend.name}`}>
              <div style={{ display: "flex", width: "100%", justifyContent: "space-around" }}>
                <div style={{ marginRight: 10 }}>
                  <FontAwesomeIcon color="#ebeb38" icon={faStar} style={{ margin: 5, fontSize: 25 }} />

                </div>
                <div style={{ width: "50%", display: "flex" }}>
                  <div style={{ margin: "auto auto auto 0", display: "flex", paddingRight: "10%" }}>
                    <img src="https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png" className="contact"></img>
                  </div>
                  <div style={{
                    width: '100%',
                    marginTop: '7px'
                  }}>
                    <h5 style={{ textAlign: "left" }}>{friend.name}</h5>
                  </div>
                </div>

                <div style={{ margin: "auto 0 auto auto", display: "flex" }}>
                  <span className=" top-0 start-100  p-2 bg-danger border border-light rounded-circle" style={{ width: 20, height: 20 }}></span>
                </div>
              </div>
            </a>
          )
        }
        )
        }


      </div>
    </div>
  )
}

export default Favorites


