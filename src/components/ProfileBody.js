import React from 'react';
import { ListGroup, ListGroupItem, CardHeader, Form, FormGroup, Label, Input, CardFooter, Modal, ModalHeader, ModalBody, ModalFooter, TabContent, TabPane, Nav, NavItem, NavLink, Row, Col, Card, CardImg, CardText, CardBody, CardTitle, CustomInput, Button } from 'reactstrap';
import classnames from 'classnames';
import avatar from "../images/avatar.png";
import { faPen, faTrash, faThumbsUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import InputField from "./InputField";
import TextArea from "./TextArea";
import services from '../services';
import { getUsername } from "../utils";

const { AuthService } = services;
class ProfileBody extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      modal: false,
      uploadImageModal: false,
      editImageModal: false,
      file: null,
      title: "",
      description: "",
      availableFor: "",
      lookingFor: props.lookingFor,
      about: props.about,
      interests: props.interests,
      ...props.info,
      likes: [],
      ...props.images,
      image: ""
    };

    this.inputRef = React.createRef();
  }

  async componentDidMount() {
    let state = { ...this.state };
    for (let key in state) {
      if (state[key] === null) {
        state[key] = ""
      }
    }
    if (state != this.state) {
      this.setState(state)
    }
  }

  range = (end) => {
    /* generate a range : [start, start+1, ..., end-1, end] */
    var len = end - 1 + 1;
    var a = new Array(len);
    for (let i = 0; i < len; i++) a[i] = 1 + i;
    return a;
  }
  toggle = (tab) => {
    this.setState({ activeTab: tab })
  }

  modalToggle = (image) => {
    const state = {modal: !this.state.modal}
    if (image) {
      state.likes = image.likes
      state.description = image.description
      state.title = image.title
      state.image = image.imgPath
      state.availableFor = image.availableFor
      state.imgId = image._id
    }

    this.setState(state)
  }
  
  editImageModalToggle = () => {
    this.setState({ editImageModal: !this.state.editImageModal })
  }

  uploadImageModalToggle = () => {
    this.setState({ uploadImageModal: !this.state.uploadImageModal })
  }

  editImage = (image) => {
    const state = {editImageModal: !this.state.editImageModal}
    if (image) {
      state.description = image.description
      state.title = image.title
      state.image = image.imgPath
      state.imgId = image._id
      state.availableFor = image.availableFor
    }
    console.log(image)
    this.setState(state)
  }

  submitEditImage = async () => {
    const data = {
      description: this.state.description,
      title: this.state.title,
      id: this.state.imgId,
      availableFor: this.state.availableFor
    }
    await AuthService.editImageInGallery(data)
    
    this.editImageModalToggle()
  }

  uploadImage = async () => {
    const formData = new FormData() 
    formData.append('file', this.state.file)
    formData.append('title', this.state.title)
    formData.append('description', this.state.description)
    formData.append('availableFor', this.state.availableFor)
    await AuthService.uploadImageToGallery(formData)
    
    this.uploadImageModalToggle()
  }

  deleteImage = async (id) => {
    await AuthService.deleteImageInGallery(id)
    window.location.reload()
  }

  setImage = (event) => {
    event.preventDefault()
    this.setState({file: event.target.files[0]})
  }

  getProfileInfo = () => {
    const info = {
      age: this.state.age,
      height: this.state.height,
      weight: this.state.weight,
      bodyType: this.state.bodyType,
      gender: this.state.gender,
      sexuality: this.state.sexuality,
      etnicity: this.state.etnicity
    }

    const data = {
      lookingFor: this.state.lookingFor,
      about: this.state.about,
      interests: this.state.interests,
      info: info
    }
    return data;
  }

  setInfo = (value, key) => {
    let info = { ...this.state }
    info[key] = value;
    this.setState(info)
  }

  setTitle = (event) => {
    event.preventDefault()
    this.setState({title: event.target.value})
  }

  setDescription = (event) => {
    event.preventDefault()
    this.setState({description: event.target.value})
  }

  setAvailableFor = (value) => {
    this.setState({availableFor: value})
  }

  likeImage = async () => {
    await AuthService.likeImage({profileName: this.props.username, id: this.state.imgId})
    this.modalToggle()
  }

  render() {


    return (
      <div style={{ marginTop: 20 }}>
        <Nav tabs>
          <NavItem style={{ cursor: "pointer" }}>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              Profil
            </NavLink>
          </NavItem>
          <NavItem style={{ cursor: "pointer" }}>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); this.props.discardEdit(); this.setState({ ...this.props.info, lookingFor: this.props.lookingFor, interests: this.props.interests, about: this.props.about }) }}
            >
              Bilder
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="8">
                <div style={{ marginLeft: 20 }}>
                  <Card style={{ marginTop: 20 }}>
                    <CardHeader tag="h5">Om mig</CardHeader>
                    <CardBody>
                      {/* <CardText>With supporting text below as a natural lead-in to additional content.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</CardText> */}
                      <CardText>{!this.props.edit ? this.state.about :
                        <TextArea category={"about"} setInfo={this.setInfo} value={this.state.about} ref={this.inputRef} type={"textarea"} />}
                      </CardText>
                    </CardBody>
                  </Card>

                  <Card style={{ marginTop: 20 }}>
                    <CardHeader tag="h5">Jag söker</CardHeader>
                    <CardBody>
                      {/* <CardText>With supporting text below as a natural lead-in to additional content.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</CardText> */}
                      <CardText>{!this.props.edit ? this.state.lookingFor :
                        <TextArea category={"lookingFor"} setInfo={this.setInfo} value={this.state.lookingFor} ref={this.inputRef} type={"textarea"} />}
                      </CardText>
                    </CardBody>
                  </Card>
                </div>
              </Col>
              <Col sm="4">

                <ListGroup style={{ textAlign: "start", marginTop: 20, marginRight: 20, marginBottom: 20 }}>
                  <CardHeader style={{ textAlign: "center" }} tag="h5">Info</CardHeader>
                  <ListGroupItem><b>Senast Inloggad: </b>{this.state.lastLoggedIn}</ListGroupItem>
                  {!this.props.edit ? <>
                    <ListGroupItem><b>Kön: </b>{this.state.gender}</ListGroupItem>
                    <ListGroupItem><b>Läggning: </b>{this.state.sexuality}</ListGroupItem>
                    <ListGroupItem><b>Etnicitet: </b>{this.state.etnicity}</ListGroupItem>
                    <ListGroupItem><b>Ålder: </b>{this.state.age} år</ListGroupItem>
                    <ListGroupItem><b>Längd: </b>{this.state.height} cm</ListGroupItem>
                    <ListGroupItem><b>Vikt: </b>{this.state.weight} kg</ListGroupItem>
                    <ListGroupItem><b>Kroppstyp: </b>{this.state.bodyType}</ListGroupItem></> :
                    <>
                      <ListGroupItem><b>Kön: </b><div>{<InputField category={"gender"} setInfo={this.setInfo} value={this.state.gender} ref={this.inputRef} type={"text"} />}</div></ListGroupItem>
                      <ListGroupItem><b>Läggning: </b><div>{<InputField category={"sexuality"} setInfo={this.setInfo} value={this.state.sexuality} ref={this.inputRef} type={"text"} />}</div></ListGroupItem>
                      <ListGroupItem><b>Etnicitet: </b><div>{<InputField category={"etnicity"} setInfo={this.setInfo} value={this.state.etnicity} ref={this.inputRef} type={"text"} />}</div></ListGroupItem>
                      <ListGroupItem><b>Ålder: </b><div>{<InputField category={"age"} setInfo={this.setInfo} value={this.state.age} ref={this.inputRef} type={"number"} />} år</div></ListGroupItem>
                      <ListGroupItem><b>Längd: </b><div>{<InputField category={"height"} setInfo={this.setInfo} value={this.state.height} ref={this.inputRef} type={"number"} />} cm</div></ListGroupItem>
                      <ListGroupItem><b>Vikt: </b><div>{<InputField category={"weight"} setInfo={this.setInfo} value={this.state.weight} ref={this.inputRef} type={"number"} />} kg</div></ListGroupItem>
                      <ListGroupItem><b>Kroppstyp: </b><div>{<InputField category={"bodyType"} setInfo={this.setInfo} value={this.state.bodyType} ref={this.inputRef} type={"text"} />}</div></ListGroupItem></>}
                </ListGroup>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            {getUsername() === this.props.username && <Row>
              <Col style={{ display: "flex" }}>
                <Button style={{ margin: "20px 0px 5px 35px" }} onClick={this.uploadImageModalToggle} color="primary" >Ladda upp bild</Button>

              </Col>
            </Row>}
            {this.props.images && this.range(Math.ceil(this.props.images.length/4)).map((n) => {
              const imgs = [...this.props.images]
              return (
            <Row style={{ margin: 20 }} key={n}>
              {imgs.slice((n-1)*4, n*4).map((image, index) => {
                  return (

                  <Col lg="3" key={index}>
                    
                    
                  <div>
                    <Card>
                    {getUsername() === this.props.username &&<div style={{display: "flex", justifyContent: "right"}}>
                    <Button style={{margin: "0.3em"}} color="warning"   onClick={() => this.editImage(image)}> <FontAwesomeIcon color="#fff" icon={faPen} /></Button>{' '}
                    <Button style={{margin: "0.3em"}} color="danger"   onClick={() => this.deleteImage(image._id)}><FontAwesomeIcon color="#fff" icon={faTrash} /></Button>{' '}
                    </div>}
                    <CardImg style={{ cursor: "pointer" }} onClick={() => this.modalToggle(image)} top width="100%" src={image.imgPath} alt={image.title} />
                      <CardBody>
                        <CardTitle tag="h5">{image.title}</CardTitle>
                        <CardText>{image.description}</CardText>
                      </CardBody>
                    </Card>
                  </div>

                  </Col>
                  )

              })}
            </Row>
              )
              
                }

              )}
              
           
          </TabPane>
        </TabContent>
        <Modal isOpen={this.state.uploadImageModal} toggle={this.uploadImageModalToggle} >
          <ModalHeader toggle={this.uploadImageModalToggle}>Ladda upp bild</ModalHeader>
          <ModalBody>
            <Form>
              <FormGroup>
                <Label for="imageTitle" >Rubrik</Label>
                  <Input  type="text" name="imageTitle" id="imageTitle" placeholder="Rubrik" onChange={this.setTitle} />
              </FormGroup>

              <FormGroup>
                <Label for="imageDescription" >Beskrivning</Label>
                  <Input  maxLength={140} type="textarea" name="imageDescription" id="imageDescription" placeholder="Beskrivning..." onChange={this.setDescription} />
              </FormGroup>
              <FormGroup>
                <Label for="imageUpload">Fil</Label>
                <CustomInput onChange={this.setImage} type="file" id="imageUpload" name="imageUpload" label="Välj fil..." />
              </FormGroup>
              <FormGroup tag="fieldset" row>
                <legend className="col-form-label">Sekretess</legend>
                <Col >
                  <FormGroup check>
                    <Label check>
                      <Input defaultChecked type="radio" name="radio2" onChange={() => this.setAvailableFor("all")} />{' '}
                      Visa för alla
                    </Label>
                  </FormGroup>
                  <FormGroup check>
                    <Label check>
                      <Input type="radio" name="radio2" onChange={() => this.setAvailableFor("favorites")} />{' '}
                      Visa för Vänner och Favoriter
                    </Label>
                  </FormGroup>
                  <FormGroup check >
                    <Label check>
                      <Input type="radio" name="radio2" onChange={() => this.setAvailableFor("friends")} />{' '}
                      Visa bara för Vänner
                    </Label>
                  </FormGroup>
                </Col>
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.uploadImage}>Skicka</Button>{' '}
            <Button color="secondary" onClick={this.uploadImageModalToggle}>Stäng</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.editImageModal} toggle={this.editImageModalToggle} >
          <ModalHeader toggle={this.editImageModalToggle}>Editera bild</ModalHeader>
          <ModalBody>
            <Form>
              <FormGroup>
                <Label for="imageTitle" >Rubrik</Label>
                  <Input value={this.state.title} type="text" name="imageTitle" id="imageTitle" placeholder="Rubrik" onChange={this.setTitle} />
              </FormGroup>

              <FormGroup>
                <Label for="imageDescription" >Beskrivning</Label>
                  <Input value={this.state.description} maxLength={140} type="textarea" name="imageDescription" id="imageDescription" placeholder="Beskrivning..." onChange={this.setDescription} />
              </FormGroup>

              <FormGroup tag="fieldset" row>
                <legend className="col-form-label">Sekretess</legend>
                <Col >
                  <FormGroup check>
                    <Label check>
                      <Input defaultChecked={this.state.availableFor == "all"} type="radio" name="radio2" onChange={() => this.setAvailableFor("all")} />{' '}
                      Visa för alla
                    </Label>
                  </FormGroup>
                  <FormGroup check>
                    <Label check>
                      <Input defaultChecked={this.state.availableFor == "favorites"} type="radio" name="radio2" onChange={() => this.setAvailableFor("favorites")} />{' '}
                      Visa för Vänner och Favoriter
                    </Label>
                  </FormGroup>
                  <FormGroup check >
                    <Label check>
                      <Input defaultChecked={this.state.availableFor == "friends"} type="radio" name="radio2" onChange={() => this.setAvailableFor("friends")} />{' '}
                      Visa bara för Vänner
                    </Label>
                  </FormGroup>
                </Col>
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.submitEditImage}>Skicka</Button>{' '}
            <Button color="secondary" onClick={this.editImageModalToggle}>Stäng</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.modal} toggle={() => this.modalToggle()} >
          <ModalHeader toggle={() => this.modalToggle()}>{this.state.title}</ModalHeader>
          <ModalBody>
            <CardImg top width="100%" src={this.state.image} alt="Card image cap" /> 
            <CardText>{this.state.description}</CardText>
            
            </ModalBody>
          <ModalFooter>
            <h3>{this.state.likes.length}</h3>
            <Button color={this.state.likes.includes(getUsername()) ? "success" : "primary" }   onClick={this.likeImage}><FontAwesomeIcon color="#fff" icon={faThumbsUp} /></Button>{' '}
            <Button color="secondary" onClick={() => this.modalToggle()}>Stäng</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }
}

export default ProfileBody;

