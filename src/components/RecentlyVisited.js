import React from 'react'
import './SideMenu.css';
import RecentlyVisitAvatar from './RecentlyVisitAvatar';

class RecentlyVisited extends React.Component {
  constructor(props) {
    super()
    this.state = {
      avatar: "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png"
    };  
  }
   
render() {
  return (
    <div style={{width: 260, height: 290, backgroundColor: "#283233"}}>
    <div style={{ display: "flex"}}>
      <h5 style={{color:"#CCC", marginLeft: "auto", marginRight: "auto", marginTop: 30}}>SENASTE BESÖKARE</h5>
      </div>
      <div>
        <div style={{display: "flex"}}>
           <RecentlyVisitAvatar avatar={this.state.avatar} name="Användare1"/>
           <RecentlyVisitAvatar avatar={this.state.avatar} name="test2"/>
           <RecentlyVisitAvatar avatar={this.state.avatar} name="tesfdsfd3"/>
      </div>
      <div style={{display: "flex"}}>
           <RecentlyVisitAvatar avatar={this.state.avatar} name="Ae1"/>
           <RecentlyVisitAvatar avatar={this.state.avatar} name="tedddst2"/>
           <RecentlyVisitAvatar avatar={this.state.avatar} name="test3"/>
      </div>
      <div style={{display: "flex"}}>
           <RecentlyVisitAvatar avatar={this.state.avatar} name="Anväfmffdsndare1"/>
           <RecentlyVisitAvatar avatar={this.state.avatar} name="tesdddt2"/>
           <RecentlyVisitAvatar avatar={this.state.avatar} name="test3dd"/>
      </div>
      </div>
      </div>    
  )
}
}

export default RecentlyVisited
