import React from 'react';
import { useEffect, useState } from 'react';
import  avatar from "../images/avatar.png";

import services from '../services';
const { AuthService } = services;


const Contacts = (props) => {
    const [friends, setFriends] = useState([])
    useEffect(() => {
        const getFriends = async () => {
          const response = await AuthService.getFriends()
          console.log(response.data.friends, response.data.friendsOnline)
          setFriends(response.data.friends)
        }
        getFriends()
      }, []);
    useEffect(() => {
      console.log(friends, friends.filter(friend => friend.online).length)
      props.methods(friends.filter(friend => friend.online).length)
    }, [friends])

  return (
    <div style={{backgroundColor: "#fff", width: "100%", minHeight: 'calc(100vh - 81px)'}}>
       <div className="list-group">
         <div className="list-group-item">
       <h1 style={{textAlign: "left"}}>Vänner</h1>
       </div>
       {friends.map((friend) => {
         return (
          <a className="list-group-item list-group-item-action" href={`/user/${friend.name}`}>
          <div style={{display: "flex", width: "100%", justifyContent: "space-around"}}>
          <div style={{width: "75%", display: "flex"}}>
          <div style={{margin:"auto auto auto 0", display: "flex", paddingRight: "10%"}}>
          <img src={friend.profilePicture ? friend.profilePicture : avatar} className="contact"></img>  
          </div>
          <div style={{width: '100%',
marginTop: '7px'}}>
          <h5 style={{textAlign: "left"}}>{friend.name}</h5>
          </div>
          <div style={{width: '100%',
marginTop: '7px'}}>
          {!friend.online && <p style={{textAlign: "left"}}><strong>Senast inloggad: </strong>{friend.lastLoggedIn}</p>}
          </div>
          </div>
          <div style={{margin:"auto 0 auto auto", display: "flex"}}>
        {friend.online && <span className=" top-0 start-100  p-2 bg-success border border-light rounded-circle" style={{width: 20, height: 20}}></span>}
        </div>
        </div>
        </a>
         )
       })}
       

      </div>
      </div>
  )
}

export default Contacts


