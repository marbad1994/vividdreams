import React, { useEffect, useState } from 'react';
import { Media } from 'reactstrap';
import { faExternalLinkSquareAlt, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { getUsername} from '../utils';
import services from '../services';

const { AuthService } = services;
const Ad = (props) => {
  const id = props.ad.id
  const history = props.history
  const { age, sexuality, user, subject, description, createdAt, adType, ageFrom, ageTo, visited, responses, city, county } = props.ad
  const avatar = "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png"
  const [profilePicture, setProfilePicture] = useState("")

  useEffect(() => {
    const getProfilePicture = async () => {
      const { data } = await AuthService.getProfilePicture(user)
      setProfilePicture(data ? data : avatar)
    }
    getProfilePicture()
  },[])
  
  return (
    <div style={{padding: "10px 20px 10px 20px"}}>
        <div  style={{backgroundColor: "#c3c3c3"}}>
        <div style={{display: "flex", justifyContent: 'space-between', backgroundColor: "#870060", borderTopLeftRadius: 5, borderTopRightRadius: 5}}><h3 style={{paddingTop: 2, marginLeft: 10, color: "#fff", marginTop: "auto", marginBottom: "auto"}}>{subject}</h3>
        <div style={{display: 'flex'}}>
        <button type="button" className="btn btn-primary transparent-btn" type="button" onClick={(e) => {e.preventDefault(); history.push(`/ad/${id}`)}} >
                  <FontAwesomeIcon data-bs-toggle="tooltip" data-bs-placement="top" title="Läs annons" color="#fff" icon={faExternalLinkSquareAlt} style={{ margin: 5, fontSize: 25 }} />
            </button>
            {user == getUsername() && <button type="button" className="btn btn-primary transparent-btn" type="button" onClick={() => props.deleteAd(id)} >
                  <FontAwesomeIcon data-bs-toggle="tooltip" data-bs-placement="top" title="Ta bort annons" color="red" icon={faTrash} style={{ margin: "5px 5px 5px 0px", fontSize: 25 }} />
            </button>}
            </div>
                  </div>
        <div style={{display: "flex", backgroundColor: "rgba(49, 59, 60, 0.94)", height: 150, color: "white", textAlign: "left"}}>
            <img style={{padding: 15, borderRadius: 20, width: 150, height: 150, objectFit: "cover"}} src={profilePicture}></img>
            <div style={{padding: 10, minWidth: "40%", marginBottom: 5}}>
              <div style={{backgroundColor: "#313b3c", borderRadius: 5, height: "100%", marginTop: 2, padding: 10}}>
                <div style={{marginTop: 10, overflow: "hidden", height: "100%", textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>
              <div style={{display: "flex", marginBottom: -7}}>
                <p style={{width: "50%", textAlign: "left"}}><strong style={{color: "#ac007a"}}>Län: </strong>{county}</p>
              <p style={{width: "50%", textAlign: "left"}}><strong style={{color: "#ac007a"}}>Stad: </strong>{city}</p>
                </div>
                <div style={{display: "flex", marginBottom: -7}}>
                <p style={{width: "50%", textAlign: "left"}}><strong style={{color: "#ac007a"}}>Användare: </strong><a style={{color: "#fff"}} href={`/user/${user}`}>{user}</a></p>
              <p style={{width: "50%", textAlign: "left"}}><strong style={{color: "#ac007a"}}>Annonstyp: </strong>{adType}</p>
                </div>
                <div style={{display: "flex", marginBottom: -7}}>
                <p style={{width: "50%", textAlign: "left"}}><strong style={{color: "#ac007a"}}>Ålder: </strong>{age}</p>
                <p style={{width: "50%", textAlign: "left"}}><strong style={{color: "#ac007a"}}>Sexuell läggning: </strong>{sexuality}</p>
                </div>
                </div>
              </div>
            </div>
            <div style={{width: "100%", padding: 10, overflow: 'hidden', textOverflow: 'ellipsis', backgroundColor: "#313b3c", marginTop: 12, marginBottom: 14, borderRadius: 5, marginRight: 12}}>
                <p style={{textOverflow: 'ellipsis'}}>
    {description}
                </p>
            </div>
            </div>
            </div>
            <div style={{backgroundColor: "#313b3c", borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
              <p style={{color: "rgb(173, 173, 173)", textAlign: "right", padding: "5px 10px 5px 10px"}}>Postad: {createdAt}</p>
            </div>
    </div>
  )
}

export default Ad


