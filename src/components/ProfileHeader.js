import React from 'react'
import { Button, ButtonGroup, Badge, FormGroup, Label, CustomInput, Input, FormText } from 'reactstrap';
import './Profile.css';
import  avatar from "../images/avatar.png";
import { faMapMarkerAlt, faStar, faUserPlus, faThumbsUp, faStopCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import services from '../services';
import { getUsername, setProfilePicture } from "../utils";

const { AuthService } = services;

class ProfileHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      counties: {},
      cities: [],
      county: props.county,
      city: props.city,
      selectedFile: null
    };
  }

  async componentDidUpdate(prevProps) {
    if (this.props.edit != prevProps.edit && this.props.edit) {
      const countyData = await AuthService.getCounties()
      this.setCounties(countyData.data)
  }
    if (this.props.edit != prevProps.edit && !this.props.edit) {
      if (this.props.save) {
        this.onClickHandler()
        this.props.setGeo(this.county, this.city)
      }
    }
  }

  getCities = async (county) => {
    this.setCounty(county)
    const countyCode = this.state.counties[county];
    const cityData = await AuthService.getCities(countyCode)
    this.setCities(cityData.data)
  }

  setCounty = (county) => {
    this.setState({county: county})
  }

  setCounties = (counties) => {
    this.setState({counties: counties})
  }

  setCity = (city) => {
    this.setState({city: city})
  }

  setCities = (cities) => {
    this.setState({cities: cities})
  }

  handleCity = (event) => {
    this.setState({city: event.target.value});
  }

  handleCounty = (event) => {
    this.setState({county: event.target.value});
  }

  onChangeHandler = (event) => {
    console.log(event.target.files[0])
    this.setState({selectedFile: event.target.files[0]})
  }

  onClickHandler = async () => {
    console.log(this.state.selectedFile)
    const data = new FormData() 
    data.append('file', this.state.selectedFile)
    for (var key of data.entries()) {
      console.log(key[0] + ', ' + key[1]);
  }
    const response = await fetch(
      "http://localhost/", {
        method: "POST",
        "Content-Type": "image/apng",
        body: data
      }
    )
    console.log(response)
  }

  upload = async () => {
    const formData = new FormData() 
    formData.append('file', this.state.selectedFile)
    formData.append('fileName', 'this.state.selectedFile')
    await AuthService.uploadProfilePicture(formData)
    const { data } = await AuthService.getProfilePicture(this.props.username)
    setProfilePicture(data)
    window.location.reload()
  }



  render() {
    const uploadContainer = {
      display: "flex",
      flexDirection: "column",
      width: "50%",
      margin: "auto auto auto 0px"
  }
  return (
    <div style={{ display: "flex", justifyContent: "space-between"}}>
    <div style={{width: "30%", display: "flex"}}>
    <img src={this.props.profilePicture ? this.props.profilePicture : avatar} style={{borderRadius: "50%", objectFit: "cover", width: 150, height: 150, marginLeft: 20, marginTop: 20,  marginBottom: "auto", marginRight: "auto", border: 2, borderColor: "gray", borderStyle: "solid"}}></img>
        {this.props.edit && <>
        <div style={uploadContainer}>
        <FormGroup>
            <Label for="imageUpload">Ladda upp profilbild</Label>
            <CustomInput type="file" id="imageUpload" name="imageUpload" onChange={this.onChangeHandler} label="Välj fil..." />
        </FormGroup>
        <Button  onClick={this.upload} color="primary" >Ladda upp</Button>
        </div>
        </>}
        </div>
    <div style={{marginRight: "auto", marginBottom: "auto", marginTop: 20, flexDirection: "row", textAlign: "start"}}>
      <h1 style={{color:"#000", fontFamily: "revert", textTransform: "capitalize" }} >{this.state.username}</h1>
      <div style={{display: "flex"}}>
      <FontAwesomeIcon color="#870060" icon={faMapMarkerAlt} style={{margin: 5}}/>
      {!this.props.edit ? <h5 style={{textTransform: "capitalize"}}>{this.state.city}, {this.state.county}</h5> :<div style={{display: "flex"}} > <div>
          <select defaultValue={this.state.county} id="counties" name="counties" onChange={(e) => this.getCities(e.target.value)}>
          {/* <option value="" selected disabled hidden>Län: </option> */}
            {Object.keys(this.state.counties).map((key, index)=>(
              <option value={key} key={index}>{key}</option>
            ))}
          </select>
          </div>
          <div>
          <select defaultValue={this.state.city} id="citites" name="cities" onChange={(e) => this.setCity(e.target.value)}>
          {/* <option value="" selected disabled hidden>Stad: </option> */}
            {this.state.cities.map((key, index)=>(
              <option value={key} key={index}>{key}</option>
            ))}
          </select>
          </div>
          </div>}
      </div>
      </div>
      <div style={{padding: 20, width: "10%", display: "flex", flexDirection: "column", alignItems: "last baseline", justifyContent: "space-evenly"}}>
      <h1><Badge style={{backgroundColor: "#870060"}}>93+</Badge></h1>
      { getUsername() !== this.props.username ?
      <ButtonGroup >
      <Button data-bs-toggle="tooltip" data-bs-placement="top" title="Gilla användare"> <FontAwesomeIcon color="#19c919" icon={faThumbsUp}/></Button>
      <Button data-bs-toggle="tooltip" data-bs-placement="top" title="Skicka vänförfrågan"><FontAwesomeIcon color="#1b1bdb" icon={faUserPlus}/></Button>
      <Button data-bs-toggle="tooltip" data-bs-placement="top" title="Spara i favoriter"><FontAwesomeIcon color="#ebeb38" icon={faStar}/></Button>
      <Button data-bs-toggle="tooltip" data-bs-placement="top" title="Blockera användare"><FontAwesomeIcon color="#f11515" icon={faStopCircle} style={{fontSize: 20, marginTop: 2.5}}/></Button>
      
    </ButtonGroup> : <Button onClick={() => this.state.toggleEdit()} color="primary" >{this.props.edit ? "Spara Profil": "Editera Profil"}</Button>}
      </div>  
      </div>
  )
}
}
// #5a595d
// gray
// 3d3d3d

export default ProfileHeader

