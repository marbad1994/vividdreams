import React from 'react'
import  avatar from "../images/avatar.png";
import './Profile.css';
import { getProfilePicture } from '../utils';

class ProfilePicture extends React.Component {
  constructor(props) {
    super();
    this.state = {
      profilePicture: undefined
    };
  this.routerRef = props.routerRef
  }

  componentDidMount() {
    const profilePicture = getProfilePicture()
    this.setState({profilePicture: profilePicture != "undefined" ? profilePicture : avatar})
    console.log("dfs", profilePicture)
  }

  render() {

  return (
    <div style={{width: 260, height: 100, backgroundColor: "#283233", display: "flex", marginTop: 15}}>
    <img src={this.state.profilePicture}  onClick={() => this.routerRef.current.history.push("/profile")} style={{objectFit: "cover", cursor: "pointer",borderRadius: "50%", width: 50, height: 50, marginTop: "auto", marginBottom: "auto", marginLeft: "auto", marginRight: 20}}></img>

      <h5 className="SideProfile" onClick={() => this.routerRef.current.history.push("/profile")} style={{ cursor: "pointer", color:"#CCC",  marginRight: "auto", marginTop: "auto", marginBottom: "auto"}}>{this.props.username}</h5>
     
      </div>
  )
  }
}

export default ProfilePicture
