import React from 'react'
import { ListGroup, ListGroupItem, Badge } from 'reactstrap';
import { faEnvelope, faStar, faUser, faAddressCard, faComment } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './SideMenu.css';
import RecentlyVisited from './RecentlyVisited';
import ProfilePicture from './ProfilePicture';
import services from '../services';

const { AuthService } = services;
class SideMenu extends React.Component {
  constructor(props) {
    super()
    this.routerRef = props.routerRef
    this.username = props.username
    this.state = {
      notice: {}
    }
  }
  async componentDidMount() {
    const { data } = await AuthService.getStats()
    this.setState({ notice: data })
  }

  render() {
    return (
      <div>

        <div class="offcanvas offcanvas-start show" style={{ visibility: "visible", width: 260, backgroundColor: "#870060", marginLeft: -0 }} data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
          <div class="offcanvas-header">
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
          </div>
          <div class="offcanvas-body" style={{ padding: 0, overflow: "hidden" }}>
            <ProfilePicture routerRef={this.routerRef} username={this.username} />
            <ListGroup style={{ width: 260, borderRadius: 0 }}>
              <ListGroupItem onClick={() => this.routerRef.current.history.push("/mail")} className="justify-content-between MenuItem"><div style={{ display: "flex" }}>
                <div>
                  <FontAwesomeIcon icon={faEnvelope} style={{ marginRight: 10 }} />MEDDELANDEN
                  </div>
                  <div style={{ marginLeft: "auto" }}>
                  {this.state.notice.unreadMails > 0 && <Badge pill>{this.state.notice.unreadMails}</Badge> }
                </div>
                </div>
                </ListGroupItem>
              <ListGroupItem onClick={() => this.routerRef.current.history.push(`/ads/${this.username}`)} className="justify-content-between MenuItem">
                <div style={{ display: "flex" }}>
                  <div>
                    <FontAwesomeIcon icon={faAddressCard} style={{ marginRight: 10 }} />MINA ANNONSER
                  </div>
                  <div style={{ marginLeft: "auto" }}>
                    {this.state.notice.userAds > 0 && <Badge pill>{this.state.notice.userAds}</Badge>}
                  </div>
                  </div>
                  </ListGroupItem>
              <ListGroupItem onClick={() => this.routerRef.current.history.push("/contacts")} className="justify-content-between MenuItem">
                <div style={{ display: "flex" }}><div><FontAwesomeIcon icon={faUser} style={{ marginRight: 10 }} />VÄNNER
                </div>
                <div style={{ marginLeft: "auto" }}>
                {this.props.onlineUsers > 0 && <Badge pill>{this.props.onlineUsers}</Badge> }
                  </div>
                  </div>
                  </ListGroupItem>
              <ListGroupItem onClick={() => this.routerRef.current.history.push("/messages")} className="justify-content-between MenuItem">
                <div style={{ display: "flex" }}>
                  <div>
                    <FontAwesomeIcon icon={faComment} style={{ marginRight: 10 }} />CHAT
                    </div>
                    <div style={{ marginLeft: "auto" }}>
                      <Badge pill>14</Badge>
                      </div>
                      </div>
                      </ListGroupItem>
              <ListGroupItem onClick={() => this.routerRef.current.history.push("/favorites")} className="justify-content-between MenuItem">
                <div style={{ display: "flex" }}>
                  <div>
                    <FontAwesomeIcon icon={faStar} style={{ marginRight: 10 }} />FAVORITER
                    </div>
                    <div style={{ marginLeft: "auto" }}>
                      <Badge pill>4</Badge>
                      </div>
                      </div>
                      </ListGroupItem>
            </ListGroup>
            <RecentlyVisited />
          </div>
        </div>
      </div>

    )
  }
}

export default SideMenu
