import React from 'react';
import { useEffect, useState } from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import { Link } from "react-router-dom";
import Chat from '../chat/chat.js';
import services from '../services';
const { AuthService } = services;


const Friends = (props) => {
    const [friends, setFriends] = useState([])
    useEffect(() => {
        setFriends(
            await AuthService.getFriends()
        )
      });

      const sendData = (friend) => {
        if (friend !== "") {
            const { roomId, name } = AuthService.getChat(friend)
          socket.emit("joinRoom", { name, roomId });
          props.routerRef.current.history.push(`/chat/${roomId}/${friend}`)
        } else {
          alert("username and roomname are must !");
        }
      };

return (
    <div>

      <h3>Friends</h3>
      <ListGroup>
          {
              friends.map((value, index) => {
                <ListGroupItem key={index} tag="button" action onClick={() => sendData(value)} >{value}</ListGroupItem>

              })
          }
        
      </ListGroup>
    </div>
  );
}

export default Friends;