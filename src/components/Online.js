import React, { useEffect, useState } from 'react';
import { ListGroup, ListGroupItem, Col, Input, Form, Row, FormGroup, Label } from 'reactstrap';

import Ad from './Ad';
import { faPlusSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import services from '../services';

const { AuthService } = services;
const Online = (props) => {
  const [counties, setCounties] = useState([])
  const [cities, setCities] = useState([])
  const [selectedCounty, setSelectedCounty] = useState("")
  const [selectedCity, setSelectedCity] = useState("")
  const [ads, setAds] = useState([])
  const [ageFromFilter, setAgeFromFilter] = useState(0)
  const [ageToFilter, setAgeToFilter] = useState(0)
  const [searchStringFilter, setSearchStringFilter] = useState("")
  const [adTypeFilter, setAdTypeFilter] = useState([])
  const [usersOnline, setUsersOnline] = useState([])
  const [newAd, setNewAd] = useState({
    city: selectedCity,
    county: selectedCounty,
    subject: "",
    description: "",
    adType: "",
    ageFrom: 18,
    ageTo: 99
  })

  useEffect(() => {
    const getCounties = async () => {
      const { data } = await AuthService.getOnlineUsers()
      setUsersOnline(data.onlineUsers)
      
      const response = await AuthService.getCounties()
      setCounties(response.data)
      await getCities(Object.keys(response.data)[0], response.data)
    }
    getCounties()
  }, [])

  const getCities = async (county, resCounties) => {
    setSelectedCounty(county)
    const countyCode = counties[county] ? counties[county] : resCounties[county];
    const cityData = await AuthService.getCities(countyCode)
    setCities(cityData.data)
    setSelectedCity(cityData.data[0])
    const ad = {...newAd}
    ad.county = county
    ad.city = cityData.data[0]
    setNewAd(ad)
  } 
  const setValue = (event, key) => {
    event.preventDefault()
    const ad = {...newAd}
    ad[`${key}`] = event.target.value
    setNewAd(ad)
  }


  const adSearch = () => {
    setAds(ads.filter(ad => ad.ageTo <= ageToFilter && ad.ageFrom >= ageFromFilter && ad.user.startsWith(searchStringFilter) && selectedCity == ad.city && selectedCounty == ad.county && adTypeFilter.includes(ad.adType)))
  }

  return (
    <div style={{backgroundColor: "rgb(249, 249, 249)", width: "100%", minHeight: 'calc(100vh - 81px)'}}>

               <div className="list-group-item" style={{marginLeft: 20, marginRight: 20, marginTop: 20}}>
                 <div style={{display: "flex"}}>
       <h1 style={{marginTop: "auto", marginBottom: "auto"}}>Medlemmar Online</h1>
      
                </div>
       </div>
       <div style={{padding: 20}}>
       <div>
       <div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
        Visa filter
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
      <Form>
  <Row form>
    <Col md={6}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="geoArea" style={{marginRight: "auto"}}>
      Län
    </Label>
    <Input
      id="geoArea"
      name="select"
      type="select"
      onChange={(e) => getCities(e.target.value)}
    >
     {counties !== undefined && Object.keys(counties).map((key, index)=>(
              <option value={key} key={index}>{key}</option>
            ))}
    </Input>
  </FormGroup>
 
    </Col>
    <Col md={3}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="ageFrom" style={{marginRight: "auto"}}>
      Ålder Från
    </Label>
    <Input
      min={18}
      id="ageFrom"
      name="number"
      placeholder={18}
      type="number"
      onChange={(e) => setAgeFromFilter(e.target.value)}
    />
  </FormGroup>
    </Col>
    <Col md={3}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="ageTo" style={{marginRight: "auto"}}>
      Ålder Till
    </Label>
    <Input
      id="ageTo"
      name="number"
      min={ageFromFilter}
      placeholder={99}
      type="number"
      onChange={(e) => setAgeToFilter(e.target.value)}
    />
  </FormGroup>
    </Col>
  </Row>
  <Row>
  <Col md={6}>
  <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="cities" style={{marginRight: "auto"}}>
      Städer
    </Label>
    <Input
      id="cities"
      multiple
      name="selectMulti"
      type="select"
      onChange={(event) => setValue(event, "city")}
    >
      { cities != null && cities.map((key, index)=>(
              <option value={key} key={index}>{key}</option>
            ))}
     
    </Input>
  </FormGroup>
    </Col>
    <Col md={6}>
    <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="addType" style={{marginRight: "auto"}}>
      Annonstyp
    </Label>
    <Input
      id="addType"
      multiple
      name="selectMulti"
      type="select"
      onChange={(e) => {const tmp = [...adTypeFilter]; tmp.push(e.target.value); setAdTypeFilter(tmp)}}
    >
     <option>Kille Söker Tjej</option>
     <option>Tjej Söker Kille</option>
     <option>Kille Söker Kille</option>
     <option>Tjej Söker Tjej</option>
     <option>Tjej Söker Trans</option>
     <option>Kille Söker Trans</option>
     <option>Trans Söker Trans</option>
     <option>Trans Söker Kille</option>

     
    </Input>
  </FormGroup>
    </Col>
  </Row>
  <Row>
    <Col md={6}>
    <FormGroup>
    <Input
      id="search"
      name="search"
      placeholder="Sök på användarnamn..."
      onChange={(e) => setSearchStringFilter(e.target.value)}
    />
  </FormGroup>
    </Col>
    <Col md={3}>
    <FormGroup
    check
  >
    <Input type="checkbox" />
    <Label check>
      Visa Bara Online
    </Label>
  </FormGroup>
    </Col>
    <Col md={3}>
    <button className='btn btn-primary' onClick={() => adSearch()} style={{width: "100%"}}>
    Sök
  </button>
    </Col>
  </Row>
  </Form>
      </div>
    </div>
  </div>

</div>
</div>

</div>
<ListGroup>
          {
            usersOnline.map((value, index) => {
                return (
                <ListGroupItem key={index} tag="button" action  >{value.name}</ListGroupItem>
                )

              })
          }
        
      </ListGroup>
       
      </div>
  )
}

export default Online


