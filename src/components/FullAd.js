import React, { useEffect, useState } from 'react'
import services from '../services';
import { Input, Form, Row, FormGroup, Label, TabContent, TabPane, Nav, NavItem, NavLink, Col, Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';
import { getUsername } from "../utils";

const { AuthService } = services;
const FullAd = (props) => {
  const id = props.match.params.id
  const avatar = "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png"
  const [ad, setAd] = useState({})
  const [message, setMessage] = useState("")
  const [recipient, setRecipient] = useState("")
  const [messageStart, setMessageStart] = useState("")
  useEffect(() => {
    const getAd = async () => {
      const { data } = await AuthService.getAd(id)
      setAd(data.ad)
      setMessageStart(`Svar på din annons "${data.ad.subject}":\n`)
      setRecipient(data.ad.user)
    }
    getAd()
  }, [])

  const sendMessage = async () => {
    await AuthService.postMessage(getUsername(), recipient, message)
  }
  return (
    <div style={{ backgroundColor: "rgb(249, 249, 249)", width: "100%", minHeight: 'calc(100vh - 81px)' }}>


      <div style={{ marginLeft: "auto" }}>
        <div style={{ padding: "10px 20px 10px 20px" }}>
          <div style={{ backgroundColor: "#c3c3c3" }}>
            <div style={{ display: "flex", justifyContent: 'space-between', backgroundColor: "#870060", borderTopLeftRadius: 5, borderTopRightRadius: 5 }}><h3 style={{ paddingTop: 2, marginLeft: 10, color: "#fff", marginTop: "auto", marginBottom: "auto" }}>{ad.subject}</h3>
            </div>
            <div style={{ display: "flex", backgroundColor: "rgba(49, 59, 60, 0.94)", height: 150, color: "white", textAlign: "left" }}>
              <img style={{ padding: 15, borderRadius: 20 }} src={avatar}></img>
              <div style={{ padding: 10, paddingRight: 20, width: "100%", marginBottom: 5 }}>
                <div style={{ backgroundColor: "#313b3c", borderRadius: 5, height: "100%", marginTop: 2, padding: 10 }}>
                  <div style={{ marginTop: 10, overflow: "hidden", height: "100%", textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>
                    <div style={{ display: "flex", marginBottom: -7 }}>
                      <p style={{ width: "50%", textAlign: "left" }}><strong style={{ color: "#ac007a" }}>Län: </strong>{ad.county}</p>
                      <p style={{ width: "50%", textAlign: "left" }}><strong style={{ color: "#ac007a" }}>Stad: </strong>{ad.city}</p>
                      <p style={{ width: "50%", textAlign: "left" }}><strong style={{ color: "#ac007a" }}>Söker i åldrarna: </strong>{ad.ageFrom} - {ad.ageTo} år</p>
                    </div>
                    <div style={{ display: "flex", marginBottom: -7 }}>
                      <p style={{ width: "50%", textAlign: "left" }}><strong style={{ color: "#ac007a" }}>Användare: </strong><a style={{ color: "#fff" }} href={`/user/${ad.user}`}>{ad.user}</a></p>
                      <p style={{ width: "50%", textAlign: "left" }}><strong style={{ color: "#ac007a" }}>Annonstyp: </strong>{ad.adType}</p>
                      <p style={{ width: "50%", textAlign: "left" }}><strong style={{ color: "#ac007a" }}>Visningar: </strong>{ad.visited}</p>
                    </div>
                    <div style={{ display: "flex", marginBottom: -7 }}>
                      <p style={{ width: "50%", textAlign: "left" }}><strong style={{ color: "#ac007a" }}>Ålder: </strong>{ad.age}</p>
                      <p style={{ width: "50%", textAlign: "left" }}><strong style={{ color: "#ac007a" }}>Sexuell läggning: </strong>{ad.sexuality}</p>
                      <p style={{ width: "50%", textAlign: "left" }}><strong style={{ color: "#ac007a" }}>Svar: </strong>{ad.responses} </p>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div style={{ display: "flex", backgroundColor: "rgba(49, 59, 60, 0.94)", height: 150, color: "white", textAlign: "left" }}>
              <div style={{ display: "flex", flexDirection: "column", width: "100%", padding: 20, backgroundColor: "#313b3c", marginBottom: 14, borderRadius: 5, margin: "10px 20px 20px 15px" }}>
                <p>
                  {ad.description}
                </p>
                <button style={{maxWidth: 200, marginLeft: 'auto'}} className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#adResponseModal">Svara på annons</button>
              </div>
            </div>
            <div class="modal fade" style={{top: "25%"}} id="adResponseModal" tabindex="-1" aria-labelledby="adResponseModalLabel" aria-hidden="true" data-bs-toggle="tooltip" data-bs-placement="top" title="Meddelande">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="adResponseModalLabel">Svara på {ad.user}'s annons</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <Form>
        <Row form>
  <Col md={12}>
  <FormGroup style={{display: 'flex',
flexDirection: 'column'}}>
    <Label for="description" style={{marginRight: "auto"}}>
      Meddelande
    </Label>
    <Input
      id="description"
      name="text"
      type="textarea"
      onChange={(e) => setMessage(messageStart + e.value.target)}

    />
  </FormGroup>
  </Col>
</Row>
  </Form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Stäng</button>
        <button type="submit" class="btn btn-primary" 
        onClick={() => sendMessage()}
        >Skicka</button>
      </div>
    </div>
  </div>
</div>
          </div>
          <div style={{ backgroundColor: "#313b3c", borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }}>
            <p style={{ color: "rgb(173, 173, 173)", textAlign: "right", padding: "5px 10px 5px 10px" }}>Postad: {ad.createdAt}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default FullAd


