import React from 'react'
import ProfileBody from './ProfileBody';
import ProfileHeader from './ProfileHeader';
import services from '../services';
import { getUsername, logout } from "../utils";

const { AuthService } = services;

class UserProfile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      profile: null,
      info: null,
      edit: false,
      username: props.username,
      originalProfile: null,
      originalInfo: null,
      save: false
    };

    this.profileBody = React.createRef();
  }

  async getUserProfile() {
    let username = getUsername()
    console.log(username)
    if (username) {
      this.props.methods(username)
      let profile = await AuthService.getProfile(this.props.match.params.username)
      if (username == this.props.match.params.username) {
        let newProfile = {...profile}
        newProfile.data.info.lastLoggedIn = Date.now().toString()
        let response = await AuthService.editProfile(username, profile.data)
        profile = response.data;
      } else {
        profile = profile.data
      }
      
      this.setState({profile: profile, info: profile.info, username: username, originalProfile: profile, originalInfo: profile.info, profilePicture: profile.profilePicture})
    }
  }

  async componentDidMount() {
      await this.getUserProfile()
  }

  toggleEdit = () => {
    this.setState({edit: !this.state.edit, save: this.state.edit})
  }

  discardEdit = () => {
    this.setState({profile: this.state.originalProfile, info: this.state.originalInfo, edit: false, save: false})
  }

  saveProfile = async(profile) => {
      await AuthService.editProfile(this.state.username, profile)
  }

  setGeo = (county, city) => {
    const { about, lookingFor, info, interests} = this.profileBody.current.getProfileInfo()
    let profile = {...this.profile}
    profile.about = about;
    profile.lookingFor = lookingFor;
    profile.info = info;
    profile.interests = interests;
    profile.county = county;
    profile.city = city;
    this.saveProfile(profile)
    this.setState({ profile: profile })
  }

  render() {
    return (
      <div style={{backgroundColor: "#fff", width: "100%", minHeight: 'calc(100vh - 81px)', marginTop: 10, marginBottom: 10}}>
        {this.state.profile !== null &&
        <>
          <ProfileHeader profilePicture={this.state.profilePicture} save={this.state.save} username={this.state.profile.username} city={this.state.profile.city} county={this.state.profile.county} toggleEdit={this.toggleEdit} edit={this.state.edit} setGeo={this.setGeo}/>
          <ProfileBody images={this.state.profile.images} username={this.state.profile.username} discardEdit={this.discardEdit} about={this.state.profile.about} lookingFor={this.state.profile.lookingFor} info={this.state.info} interests={this.state.profile.interests} edit={this.state.edit} ref={this.profileBody}/>
        </>
        }
        </div>
    )
  }
}

export default UserProfile;


