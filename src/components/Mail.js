import React, { useEffect, useState } from 'react';
import { Input, Form, Row, FormGroup, Label, TabContent, TabPane, Nav, NavItem, NavLink, Col, Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';
import classnames from 'classnames';
import { getUsername } from "../utils";
import  avatar from "../images/avatar.png";

import { faPlusSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import services from '../services';

const { AuthService } = services;
const Mail = (props) => {
  const [activeTab, toggle] = useState('1')
  const [friends, setFriends] = useState([])
  const [message, setMessage] = useState("")
  const [recipient, setRecipient] = useState("")
  const [messages, setMessages] = useState([])
  const [conv, setConv] = useState([])
  const [profilePictures, setProfilePictures] = useState({})
  useEffect(() => {
    const mount = async () => {
      const conversations = await AuthService.getConversations()
      if (conversations) {
        setConv(conversations.data.conversations)
      }
      const response = await AuthService.getFriends()
      console.log(response.data.friends)
      setFriends(response.data.friends)
    }
    mount()
  }, [])

  useEffect(() => {
    const getProfilePictures = async () => {
      const profilePictureObj = {}
      for (let i = 0; i < friends.length; i++) {
        const name = friends[i].name
        const { data } = await AuthService.getProfilePicture(name)
        profilePictureObj[name] = data ? data : avatar
      }
      setProfilePictures(profilePictureObj)
      console.log(profilePictureObj)
    }
    getProfilePictures()
  }, [friends])
  const getMessages = async (recipient) => {
    const { data } = await AuthService.getMessages(recipient)
    setMessages(data.messages)
  }
  const sendMessage = async (fromModal) => {
    let tmpRec = recipient
    if (fromModal) {
      tmpRec = document.getElementById("exampleDataList").value
    }
    await AuthService.postMessage(getUsername(), tmpRec, message)
    setMessage("")
    getMessages(tmpRec)
  }
  
  return (
    <div style={{ backgroundColor: "rgb(249, 249, 249)", width: "100%", minHeight: 'calc(100vh - 81px)' }}>
      <div className="modal fade" style={{ top: "25%" }} id="newMessageModal" tabindex="-1" aria-labelledby="newMessageModalLabel" aria-hidden="true" data-bs-toggle="tooltip" data-bs-placement="top" title="Meddelande">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="newMessageModalLabel">Lägg in annons</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <Form>
                <Row form>
                  <label for="exampleDataList" className="form-label">Användare</label>
                  <input className="form-control" list="datalistOptions" id="exampleDataList" placeholder="Användare" />
                  <datalist id="datalistOptions">
                    {friends.map((value) => {
                      return <option value={value.name} />
                    })}
                  </datalist>
                </Row>
                <Row form>
                  <Col md={12}>
                    <FormGroup style={{
                      display: 'flex',
                      flexDirection: 'column'
                    }}>
                      <Label for="description" style={{ marginRight: "auto" }}>
                        Meddelande
                      </Label>
                      <Input
                        id="description"
                        name="text"
                        type="textarea"
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}

                      />
                    </FormGroup>
                  </Col>
                </Row>
              </Form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Stäng</button>
              <button type="submit" className="btn btn-primary"
                onClick={() => sendMessage(true)}
              >Skicka</button>
            </div>
          </div>
        </div>
      </div>
      <div className="list-group-item" style={{ backgroundColor: "#283233", marginLeft: 20, marginRight: 20, marginTop: 20 }}>
        <div style={{ display: "flex" }}>
          <h1 style={{ marginTop: "auto", marginBottom: "auto", color: "white" }}>Meddelanden</h1>
          <div style={{ marginLeft: "auto" }}>
            <button type="button" className="btn btn-primary transparent-btn" type="button" data-bs-toggle="modal" data-bs-target="#newMessageModal">
              <FontAwesomeIcon color="#870060" className="icon-btn" icon={faPlusSquare} />
            </button>

          </div>
        </div>
        <div style={{ padding: 40, display: "flex", minHeight: "calc(100vh - 200px)" }}>
          <Nav tabs vertical style={{ width: '20%' }}>
            {conv.map((convObj, index) => {
              return (
                <NavItem style={{ cursor: "pointer" }} key={index}>
                  <NavLink
                    className={classnames({ active: activeTab === `${index + 1}` })}
                    onClick={() => { toggle(`${index + 1}`); }}
                    style={{ backgroundColor: "#313b3c" }}
                    onClick={() => { convObj.read = true; getMessages(convObj.name); setRecipient(convObj.name) }}
                  >
                    <div>
                      <div style={{ display: 'flex', justifyContent: 'space-between', color: "#fff" }}>
                      <img src={profilePictures[convObj.name]} style={{ objectFit: "cover", cursor: "pointer",borderRadius: "50%", width: 50, height: 50, margin: "auto auto 1em 0em"}}></img>
                        <h5 >{convObj.name}</h5>
                        <div style={{width: "10%", display: "flex" }}>
                          {!convObj.read && <span className=" top-0 start-100  p-2 bg-danger border border-light rounded-circle" style={{ width: 20, height: 20 }}></span>}
                        </div>
                      </div>
                      <p style={{ color: "#fff", textAlign: "left" }}>{convObj.date}</p>
                    </div>
                  </NavLink>
                </NavItem>
              )
            })}

          </Nav>
          <TabContent activeTab={activeTab} style={{ width: '80%', marginLeft: 40 }}>
            <TabPane tabId="1" style={{ display: "flex", flexDirection: "column", height: "calc(100vh - 200px)" }}>
              <div style={{padding: '30px', overflowY: 'scroll', overflowX: 'hidden'}}>
                {messages.map((messageObj, index) => {
                  if (messageObj.name == getUsername()) {
                    return (
                      <Row key={index}>
                        <div style={{
                          padding: '20px',
                          color: 'white',
                          backgroundColor: 'gray',
                          borderRadius: '10px',
                          marginLeft: 'auto',
                          marginBottom: 20,
                          textAlign: 'left'
                        }}>
                          <p><strong>Du</strong></p>
                          {messageObj.message.split("\n").map((row, i) => {
                            return <p style={{ lineHeight: 1.3, wordBreak: "break-all" }} key={i}>{row}</p>
                          })}

                          <p style={{
                            color: 'white',
                            fontSize: '0.8em',
                            marginBottom: '-17px',
                            textAlign: 'right'
                          }}>{messageObj.date}</p>
                        </div>




                      </Row>
                    )
                  } else {
                    return (
                      <Row key={index}>
                        <div style={{
                          padding: '20px',
                          color: 'white',
                          backgroundColor: '#313B3C',
                          borderRadius: '10px',
                          marginLeft: 'auto',
                          marginBottom: 20,
                          textAlign: 'left'
                        }}>
                          <p><strong>{messageObj.name}</strong></p>
                          {messageObj.message.split("\n").map((row, i) => {
                            return <p style={{ lineHeight: 1.3, wordBreak: "break-all" }} key={i}>{row}</p>

                          })}
                          <p style={{
                            color: 'white',
                            fontSize: '0.8em',
                            marginBottom: '-17px',
                            textAlign: 'right'
                          }}>{messageObj.date}</p>

                        </div>

                      </Row>
                    )
                  }

                })}
              </div>
              {recipient && <div style={{ marginTop: "auto" }}>
                <Form>
                  <Row form>
                    <Col md={12}>
                      <FormGroup style={{
                        display: 'flex',
                        flexDirection: 'column'
                      }}>
                        <Label for="modalMessage" style={{ marginRight: "auto", color: "#fff" }}>
                          Meddelande
                        </Label>
                        <Input
                          id="modalMessage"
                          name="text"
                          type="textarea"
                          value={message}
                          onChange={(e) => setMessage(e.target.value)}

                        />
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
                <button type="submit" className="btn btn-primary" style={{ width: "100%" }}
                  onClick={() => sendMessage()}
                >Skicka</button>
              </div>}
            </TabPane>

          </TabContent>


        </div>
      </div>

    </div>
  )
}

export default Mail


