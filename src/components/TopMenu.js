import React, { useState, useEffect } from 'react';
import { Switch, Route, Link, BrowserRouter as Router } from "react-router-dom";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';
import { logout } from '../utils';
import services from '../services';

const { AuthService } = services;
const TopMenu = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const [username, setUsername] = useState(props.username)
  
  useEffect(() => {
    setUsername(props.username)
  }, [props.username])

  const logoutHandler = async () => {
    await logout()
    await AuthService.logout()
    props.history.push("/login")
  }

  return (
    <div>
      <Navbar color="light" light expand="md" style={{zIndex: 2000, position: "fixed", width: "100%"}}>
        <img src='/images/vd-logo.png' style={{width: 300, paddingRight: 20}}/>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <Link className="nav-link" to="/news">Nyheter</Link>
            </NavItem>
            { username &&
            <>
            <NavItem>
              <Link className="nav-link" to="/users-online">Inloggade</Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" to="/ads">Annonser</Link>
            </NavItem>
            </>
            }
          </Nav>
          { username &&
          <NavLink className="nav-link" onClick={logoutHandler} href="/login">Logga ut</NavLink>
          }

        </Collapse>
      </Navbar>
      
    </div>
  );
}

export default TopMenu;