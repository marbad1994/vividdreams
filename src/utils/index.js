const TOKEN_KEY = 'jwt';
const USER_KEY = 'user';
const TIMESTAMP_KEY = 'lastLoggedIn';
const EXPIRES_AT = 'expiresAt'
const PROFILE_PICTURE = "profile"

export const login = async (token, username, expiresAt, profile) => {
    localStorage.setItem(TOKEN_KEY, token);
    localStorage.setItem(USER_KEY, username);
    localStorage.setItem(TIMESTAMP_KEY, Date.now().toString())
    localStorage.setItem(EXPIRES_AT, expiresAt)
    localStorage.setItem(PROFILE_PICTURE, profile.profilePicture)
}

export const logout = async () => {
    localStorage.removeItem(TOKEN_KEY);
    localStorage.removeItem(USER_KEY);
    localStorage.removeItem(TIMESTAMP_KEY);
    localStorage.removeItem(EXPIRES_AT);
    localStorage.removeItem(PROFILE_PICTURE);
}

export const isLogin = () => {
    if (localStorage.getItem(TOKEN_KEY)) {
        return true;
    }
    return false;
}

export const getToken = () => {
    return localStorage.getItem(TOKEN_KEY)
}

export const getUsername = () => {
    return localStorage.getItem(USER_KEY)
}

export const getLastLogin = () => {
    return localStorage.getItem(TIMESTAMP_KEY)
}

export const getExpiration = () => {
    return localStorage.getItem(EXPIRES_AT)
}

export const getProfilePicture = () => {
    return localStorage.getItem(PROFILE_PICTURE)
}

export const setProfilePicture = (profilePicture) => {
    localStorage.setItem(PROFILE_PICTURE, profilePicture)
}

export const refreshToken = (token, expiresAt) => {
    localStorage.setItem(TOKEN_KEY, token);
    localStorage.setItem(EXPIRES_AT, expiresAt)
}