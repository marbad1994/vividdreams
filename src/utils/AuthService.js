import API from "./Api"
import APIFORMDATA from "./ApiFormData"

export default class AuthService {
	constructor() {
		this.API = new API()
		this.APIFormData = new APIFORMDATA()
	}


    login = (name, password) => this.API.post("/auth/login", { name: name, password: password });

    logout = () => this.API.get("/auth/logout");

    refreshToken = () => this.API.get("/auth/refresh-token");

    getOnlineUsers = () => this.API.get("/auth/get-online-users");

    getOnlineUser = (name) => this.API.postAd("/auth/get-online-user", {name: name});

    getStats = () => this.API.get("/auth/get-stats");

    register = (name, password, email, county, city, sexuality, gender, age) => this.API.post("/auth/register", { name: name, password: password, email: email, county: county, city: city, sexuality: sexuality, gender: gender, age: age})

    getFriends = () => this.API.get("/friends/get-friends")
    
    getChat = (friend) => this.API.post("/chat/get-chat", { friend: friend })

    saveChat = (roomname, chat) => this.API.post("/chat/save-chat", {roomname: roomname, chat: chat})

    getProfile = (username) => this.API.post("/profile/get-profile", {username: username})

    editProfile = (username, profile) => this.API.post("/profile/edit-profile", {username: username, profile: profile})

    getCounties = () => this.API.get("/geo/get-counties")

    getCities = (countyCode) => this.API.post("/geo/get-cities", {countyCode: countyCode})

    getMe = () => this.API.get("/auth/me")

    uploadProfilePicture = (data) => this.APIFormData.post("/images/upload-profile-picture", data)

    postAd = (ad) => this.API.post("/ads/add-ad", {ad: ad})

    getAds = () => this.API.get("/ads/get-ads")

    getAd = (id) => this.API.post("/ads/get-ad", {id: id})

    deleteAd = (id) => this.API.delete("/ads/delete-ad", {id: id})

    getUsersAds = (username) => this.API.post("/ads/get-users-ads", {username: username})

    postMessage = (username, recipient, message) => this.API.post("/message/send-message", {username: username, recipient: recipient, message: message})

    getMessages = (recipient) => this.API.post("/message/get-messages", {recipient: recipient})

    getConversations = () => this.API.get("/message/get-conversations")

    getProfilePicture = (name) => this.API.get(`/profile/profile-picture/${name}`)

    getGallery = (name) => this.API.get(`/profile/image-gallery/${name}`)

    uploadImageToGallery = (data) => this.APIFormData.post("/profile/upload-image-to-gallery", data)
  
    deleteImageInGallery = (id) => this.API.post("/profile/delete-image-in-gallery", {id: id})

    editImageInGallery = (data) => this.API.post("/profile/edit-image-in-gallery", data)

    likeImage = (data) => this.API.post("/profile/like-image", data)


}