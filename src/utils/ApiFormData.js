// import { getToken, logout } from './index';
import { logout, getToken } from './index';

export default class APIFORMDATA {
	http = async (method, url, params) => {
		// let headersContent = { 
		// 	'Content-Type': 'multipart/form-data',
		// 	'Accept': 'multipart/form-data',
		// 	'Authorization': getToken()
		// }
		const token = getToken()
        let headersContent = {
            Authorization: `Bearer ${token}`,
        }
        let headers = new Headers(headersContent);
		let baseUrl;
    switch (process.env.NODE_ENV) {
			case "development":
					baseUrl = "http://localhost:5000/api/v1"
					break;
			default:
					baseUrl = "https://vividdreams.herokuapp.com/api/v1" // Byt denna
					break;
    }
		// let headers = new Headers(headersContent) - Can't use headers right now
		let response
		let apiUrl = `${baseUrl}${url}`
	var isFormDataEmpty= true;
		for (var p of params) {
		   isFormDataEmpty= false;
		   break;
		}
		console.log(isFormDataEmpty)
    response = await fetch(`http://localhost:5000/api/v1${url}`, {
			method,
			headers: headers,
			body: params
    });
		
    if (response.status === 500 || response.status === 301 || response.status === 401) {
			// logout()
			// window.location.href = "/admin"
			return {data: {}}
    }
    else if (response.status === 200) {
			let r = await response.json()
			if (r.status === undefined || r.status !== 200) {
				return {data: {}}

			} else {
				try {
					return r 
        } catch (error) {
			return {data: {}}

				}
			}
    }
		return {data: {}}
  }
	get = (url) => this.http("GET", url)

	post = (url, params) => this.http("POST", url, params)
	
	del = (url, params) => this.http("DELETE", url, params)
	
	put = (url, params) => this.http("PUT", url, params)

}
