import { logout, getToken } from './index';

export default class API {

    http = async(method, url, params) => {
        const token = getToken()
        let headersContent = {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json"
        }
        let headers = new Headers(headersContent);
        // let response = await fetch("http://localhost:5000" + url, {
        let response = await fetch(`http://localhost:5000/api/v1${url}`, {
            method,
            headers: headers,
            body: JSON.stringify(params)
        });

        console.log(response)

        if (response.status === 200) {
            try {
                return await response.json();
            } catch (error) {
                throw error;
            }
        } else if (response.status === 500 || response.status === 401) {
            logout()
            return { data: "error" }
        }
    }
    get = (url) => this.http("GET", url);

    post = (url, params) => this.http("POST", url, params);

    delete = (url, params) => this.http("DELETE", url, params);

    put = (url, params) => this.http("PUT", url, params);
}