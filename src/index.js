import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import rootReducers from "./store/reducer/index";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import io from "socket.io-client";

const store = createStore(rootReducers);
const routerRef = React.createRef()
const socket = io("http://localhost:5000")
ReactDOM.render(
  // <React.StrictMode>
     <Provider store={store}>
     <Router ref={routerRef} >
    <App routerRef={routerRef} socket={socket}/>
    </Router>
  </Provider>,
  // </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
