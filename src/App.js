import React from 'react';
import SideMenu from './components/SideMenu';
import RecentlyVisited from './components/RecentlyVisited';
import ProfilePicture from './components/ProfilePicture';
import TopMenu from './components/TopMenu';
import Profile from './components/Profile';
import UserProfile from './components/UserProfile';
import Ads from './components/Ads';
import FullAd from './components/FullAd';
import Contacts from './components/Contacts';
import { Switch, Route, Redirect, BrowserRouter as Router } from "react-router-dom";
import Login from './Home/Login.js';
import Register from './Home/Register.js';
import './App.css';
import "./App.scss";
import services from './services';
import PublicRoute from './components/PublicRoute';
import PrivateRoute from './components/PrivateRoute';
import Messages from './components/Messages';
import Mail from './components/Mail';
import Online from './components/Online';
import Favorites from './components/Favorites';
import { withRouter } from 'react-router';
import { getUsername, getExpiration, refreshToken } from './utils';


const { AuthService } = services;
 
class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      profile: {},
      info: {},
      username: "",
      onlineUsers: 0
    };
    this.routerRef = props.routerRef
  }

  componentDidMount() {
    const username = getUsername()
    this.setUsername(username)
  }

  setUsername = (username) => {
      this.setState({username: username})
  }

  setOnlineUsers = (onlineUsers) => {
    this.setState({onlineUsers})
  }

  componentDidUpdate() {
    const expiresAt = getExpiration()
    if (expiresAt < new Date()) {
      const { token, expiresAt } = AuthService.refreshToken()
      refreshToken(token, expiresAt)
    }
  }

  render () {
    return (
      <div>
      <TopMenu history={this.props.history} username={this.state.username}/>

    <div style={{minHeight: 650, display: "flex", backgroundColor: "rgb(204, 204, 204)"}}>
    {this.state.username &&
    <div style={{backgroundColor: "#870060", width: 250, height: window.height, paddingBottom: 80}}>
    {/* <ProfilePicture routerRef={this.routerRef} username={this.state.username}/> */}
    <SideMenu routerRef={this.routerRef} username={this.state.username} onlineUsers={this.state.onlineUsers}/>
    {/* <RecentlyVisited/> */}
    </div>
    }
    <div className="App" style={{backgroundColor: "#ccc", width: "100%", padding: 10, height: "100%", marginLeft: "3.2em", marginTop: "3.7em"}}>
      {/* App Content */}
      <Switch>
        <PrivateRoute component={Profile} methods={this.setUsername} path="/" exact />
        <PrivateRoute component={Profile} methods={this.setUsername} path="/profile" exact />
        <PrivateRoute component={UserProfile} methods={this.setUsername} path="/user/:username" />
        <PrivateRoute component={Ads} history={this.props.history} path="/ads/:username" />
        <PrivateRoute component={Ads} history={this.props.history} path="/ads" exact />
        <PrivateRoute component={Online} history={this.props.history} path="/users-online" exact />
        <PrivateRoute component={FullAd} history={this.props.history} path="/ad/:id" />
        <PrivateRoute component={Contacts} methods={this.setOnlineUsers} history={this.props.history} path="/contacts" exact />
        <PrivateRoute component={Favorites} history={this.props.history} path="/favorites" exact />
        <PrivateRoute component={Messages} history={this.props.history} socket={this.props.socket} path="/messages" exact/>
        <PrivateRoute component={Mail} history={this.props.history} socket={this.props.socket} path="/mail" exact/>
        <PrivateRoute component={Messages} history={this.props.history} socket={this.props.socket} path="/chat/:roomname/:username"/>
        <PublicRoute restricted={false} history={this.props.history} component={Login} path="/login" exact />
        <PublicRoute restricted={false} history={this.props.history} component={Register} path="/register" exact />


      </Switch>
    </div>
    </div>
    </div>
  );
  }
}

export default withRouter(App);
